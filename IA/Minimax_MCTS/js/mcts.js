/* 
    Classe Noeud nécessaire à la classe Arbre
    Données membres :
        plateau : instance de la classe Chesz, contenant toutes les données du jeu à un instant précis.
        move : mouvement permettant d'accéder au noeud actuel depuis le parent. null signifie que le noeud est la racine de l'arbre.
        gagnant : nombre de fois où ce noeud a été trouvé gagnant par l'algorithme MCTS.
        parcouru : nombre de fois où ce noeud a été parcouru par l'algorithme MCTS.
        score : score attribué au noeud en fonction de plusieurs paramètres ; 
                formule de calcul du score : w/n + c * sqrt (ln(N) / n)
                w : nombre de simulations gagnantes de ce noeud
                n : nombre total de simulations de ce noeud
                c : paramètre d'exploration, ici racine carrée de 2
                N : nombre total de simulations du noeud parent au noeud étudié
*/

class Noeud {
    constructor(fen) {
        this.plateau = new Chesz(fen);
        this.move = null;
        this.gagnant = 0;
        this.parcouru = 0;
        this.score = 0;
    }
}

/*
    Classe Arbre permettant l'exécution de l'algorithme MCTS
    Données membres :
        node : instance de la classe Noeud contenant les informations du jeu avec le nombre de combinaisons gagnantes et parcourues.
        enfants : tableau contenant n arbres qui sont les enfants de ce noeud. Un tableau vide signifie que ce noeud est une feuille.
        parent : copie de l'arbre parent à cet arbre, permettant des références aux informations nécessaires pour l'IA. 
    Fonctions membres :
        ajoutEnfant(arbre) : ajoute un enfant à l'arbre, en prenant en paramètre l'arbre à ajouter. L'arbre enfant est mis à jour pour que son attribut "parent" corresponde.
        estFeuille() : renvoie un booléen indiquant si l'arbre possède des enfants ou non.
*/

class Arbre {
    constructor(noeud = new Noeud()) {
        this.node = noeud;
        this.enfants = [];
        this.parent = null;
    }

    ajoutEnfant(arbre, mouvement) {
        this.enfants.push(arbre);
        this.enfants[this.enfants.length - 1].parent = this;
        this.enfants[this.enfants.length - 1].node.move = mouvement;
    }

    estFeuille() {
        return this.enfants.length === 0;
    }
}


//  Fonctions remplissant les différentes parties de l'algorithme MCTS

/*
    Fonction MCTSselection
    Préconditions : arbre doit être une instance de la classe Arbre.
    Données : arbre : arbre entier dans lequel choisir le noeud à étudier.
    Résultat : arbre que l'on souhaite étudier.
    Description : Fonction récursive sélectionnant le noeud à analyser lors du prochain cycle de l'algorithme MCTS.
    Variables locales :
        bestChild : arbre de l'enfant ayant le score le plus élevé.
        maxChildScore : score maximal de tous les enfants.
*/

var MCTSselection = function (arbre) {
    if (arbre.estFeuille()) {
        return arbre;
    }
    else {
        var bestChild = null;
        var maxChildScore = 0;
        for (var i = 0; i < arbre.enfants.length; i++) {
            if (maxChildScore < arbre.enfants[i].node.score) {
                maxChildScore = arbre.enfants[i].node.score;
                bestChild = arbre.enfants[i];
            }
        }
        if ((arbre.enfants.length < arbre.node.plateau.moves().length) && (arbre.node.score > maxChildScore)) {
            return arbre;
        }
        else {
            return MCTSselection(bestChild);
        }
    }
}

/*
    Fonction MCTSexpansion
    Préconditions : arbre doit être une instance de la classe Arbre ; le nombre d'enfants avant l'expansion doit être inférieur au nombre de mouvement possible à partir de l'arbre qu'on étend.
    Données : arbre : arbre à étendre.
    Résultat : arbre étendu.
    Description : fonction ajoutant un enfant à l'arbre en paramètre.
    Variables locales :
        tabMoves : tableau des mouvements possibles à partir du plateau stocké dans la racine de l'arbre.
        nbEnfantsExistants : nombre d'enfants de l'arbre déjà ajoutés.
*/

var MCTSexpansion = function (arbre) { //entrée : arbre à étendre ; sortie : arbre étendu
    var tabMoves = arbre.node.plateau.moves();
    var nbEnfantsExistants = arbre.enfants.length;

    if (tabMoves.length > nbEnfantsExistants) {
        var gameTemp = new Chesz(arbre.node.plateau.fen());
        gameTemp.move(tabMoves[nbEnfantsExistants]);
        var tempNode = new Noeud (gameTemp.fen());
        var tempTree = new Arbre (tempNode);

        arbre.ajoutEnfant(tempTree, tabMoves[nbEnfantsExistants]);
    }
    else {
        console.log("Impossible d'ajouter un enfant à cet arbre ; tous les enfants correspondant à un mouvement possible ont déjà été ajoutés.");
    }

    return arbre;
}

/*
    Fonction MCTSsimulation
    Préconditions : arbre doit être une instance de la classe Arbre.
    Données : arbre : arbre à partir duquel on effectue la simulation aléatoire.
    Résultat : aucun.
    Description : fonction simulant une partie aléatoire à partir d'un état de jeu stocké à la racine de l'arbre passé en paramètre, et met à jour les données membres gagnant et parcouru pour correspondre au résultat de la simulation.
    Variables locales :
        arbreSim : copie de l'arbre passé en paramètre pour simuler une partie sans modifier l'arbre d'origine.
        moves : mouvements possibles à un état de jeu donné.
        move : mouvement choisi aléatoirement dans le tableau moves.
*/

var MCTSsimulation = function (arbre) { //entrée : nouvelle feuille pas encore simulée ; sortie : void
    var arbreSim = new Arbre (new Noeud (arbre.node.plateau.fen()));
    while (!arbreSim.node.plateau.game_over()) {
        var moves = arbreSim.node.plateau.moves();
        var move = moves[Math.floor(Math.random() * moves.length)];
        arbreSim.node.plateau.move(move);
    }

    arbre.node.parcouru ++;

    if (arbreSim.node.plateau.in_draw()) {
        arbre.node.gagnant += 0.5;
    }
    else if (arbreSim.node.plateau.turn() === 'w') {
        arbre.node.gagnant ++;
    }
}

/*
    Fonction MCTSbackpropagationRecursif
    Préconditions : arbre doit être une instance de la classe Arbre.
    Données : 
        arbre : arbre dont les données membres n'ont pas encore été mises à jour.
        gagnant : nombre de simulations réussies dans la précédente simulation.
        parcouru : nombre total de simulation au cours de la boucle.
    Résultat : arbre avec modifications des données membres.
    Description : fonction récursive qui met à jour tout l'arbre en fonction de la simulation qui vient d'être effectuée.
    Variables locales : aucune.
*/

var MCTSbackpropagationRecursif = function (arbre, gagnant, parcouru) {
    if (!(arbre.parent === null)) {
        arbre.parent.node.gagnant += gagnant;
        arbre.parent.node.parcouru += parcouru;
        MCTSbackpropagationRecursif(arbre.parent, gagnant, parcouru);
    }
    return arbre;
}

/*
    Fonction MCTSbackpropagation
    Préconditions : arbre doit être une instance de la classe Arbre.
    Données : arbre : arbre dont les données membres n'ont pas encore été mises à jour.
    Résultat : arbre avec modifications des données membres.
    Description : fonction qui met à jour tout l'arbre en fonction de la simulation qui vient d'être effectuée.
    Variables locales : nbGagnants : nombre de simulations gagnantes lors du dernier cycle de simulations.
*/

var MCTSbackpropagation = function (arbre) { 
    var nbGagnants = 0;
    for (var i = 0; i < arbre.enfants.length; i++) {

        nbGagnants += arbre.enfants[i].node.gagnant;
    }
    arbre.node.gagnant += nbGagnants;
    arbre.node.parcouru += arbre.enfants.length;
    return MCTSbackpropagationRecursif(arbre, nbGagnants, arbre.enfants.length);
}

/*
    Fonction MCTSscore
    Préconditions : arbre doit être une instance de la classe Arbre.
    Données : arbre : arbre dont les données membres n'ont pas encore été mises à jour.
    Résultat : arbre avec modifications de la donnée membre score pour tous les noeuds.
    Description : fonction récursive parcourant l'arbre en profondeur pour mettre à jour la donnée membre score de chaque noeud en fonction des nouvelles informations apportées par le dernier cycle de simulations.
                Rappel : score : score attribué au noeud en fonction de plusieurs paramètres ; 
                        formule de calcul du score : w/n + c * sqrt (ln(N) / n)
                        w : nombre de simulations gagnantes de ce noeud
                        n : nombre total de simulations de ce noeud
                        c : paramètre d'exploration, ici racine carrée de 2
                        N : nombre total de simulations du noeud parent au noeud étudié
    Variables locales : aucune.
*/

var MCTSscore = function (arbre) {
    if (arbre.parent != null) {
        arbre.node.score = (arbre.node.gagnant / arbre.node.parcouru) + Math.SQRT2 * (Math.sqrt(Math.log(arbre.parent.node.parcouru) / arbre.node.parcouru));
    }
    for (var i = 0; i < arbre.enfants.length; i++) {
        MCTSscore(arbre.enfants[i]);
    }

    return arbre;
}

/*
    Fonction MCTSfin
    Préconditions : arbre doit être une instance de la classe Arbre ; au moins une simulation du jeu doit avor été effectuée.
    Données : arbre : arbre contenant toutes les simulations.
    Résultat : chaîne de caractères représentant le meilleur mouvement à effectuer pour le prochain coup. Exemple : "Nc6".
    Description : fonction parcourant l'arbre sur un niveau uniquement, et retournant le mouvement du noeud qui a été le plus parcouru (donc le plus prometteur d'après le fonctionnement de l'algorithme).
    Variables locales :
        bestScore : plus grand nombre de simulations parmi tous les enfants (directs) de l'arbre.
        bestMove : chaîne de caractère du mouvement lié au noeud le plus parcouru.
*/

var MCTSfin = function (arbre) {
    var bestScore = 0;
    var bestMove = null;

    for (var i = 0; i < arbre.enfants.length; i++) {
        if (bestScore < arbre.enfants[i].node.parcouru) {
            bestScore = arbre.enfants[i].node.parcouru;
            bestMove = arbre.enfants[i].node.move;
        }
    }

    return bestMove;
}

/*
    Fonction MCTS
    Préconditions : 
        - fen doit être une chaîne de caractères représentant un état de jeu d'échecs valide, sous la forme standardisée FEN.
        - diff doit être strictement positive, et entière.
    Données :
        fen : chaîne de caractères représentant un état de jeu d'échecs dans une certaine configuration, sour la forme FEN.
        diff : difficulté souhaitée pour la recherche du prochain mouvement. Doit être entière et strictement supérieure à 0.
    Résultat : chaîne de caractères du mouvement le plus prometteur.
    Description : fonction agrégeant toutes les fonctions définies si dessus, pour l'exécution de l'algorithme MCTS avec une difficultée passée en paramètres.
    Variables locales :
        game : instance de la classe Chesz initialisée avec fen.
        arbre : instance de la classe Arbre qui servira à l'exécution de l'algorithme, avec comme racine le Noeud contenant game.
        arbre2 : arbre sélectionné par la fonction MCTSselection à chaque passage dans la boucle.
*/

var MCTS = function (fen, diff) {
    var game = new Chesz(fen);
    var arbre = new Arbre (new Noeud (game.fen()));

    for (var i = 0; i < arbre.node.plateau.moves().length; i++) {
        MCTSexpansion(arbre);
        MCTSsimulation(arbre.enfants[i]);
    }
    MCTSbackpropagation(arbre);
    MCTSscore(arbre);

    for (var temps = 0; temps < arbre.node.plateau.moves().length * diff; temps++) {
        var arbre2 = MCTSselection(arbre);
        if (arbre2.node.plateau.moves().length === 0) {
            MCTSsimulation(arbre2);
        }
        else {
            MCTSexpansion(arbre2);
            var enfantAParcourir = arbre2.enfants.length - 1;
            MCTSsimulation(arbre2.enfants[enfantAParcourir]);
        }
        MCTSbackpropagation(arbre2);
        MCTSscore(arbre);
    }

    return MCTSfin(arbre);
}

var game = new Chesz();
