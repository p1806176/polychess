# Jeu d'échecs

Projet réalisé par l'équipe Frontend(à spécifier) et l'équipe IA

Site permettant de joueur aux échecs contre différentes IA avec choix de la difficulté

## Librairies

- [Bootstrap](https://getbootstrap.com/)
- [jQuery](https://jquery.com/)

### Installation et acces

#### Il faut cloner le repository dans le répertoire voulu

```
git clone https://forge.univ-lyon1.fr/p1803635/info-3-projet-peip.git FOLDER_NAME
```

#### Acces au site

Double cliquer sur le fichier html voulu permet d'accéder au site.

