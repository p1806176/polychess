create table chess (
    idPartie int auto_increment
        primary key,
    joueur1  varchar(250) not null,
    joueur2  varchar(250) not null,
    temps    double       not null,
    nbCoups  int          not null,
    gagnant  int          null
);

create table historique (
    idPartie int          not null,
    idCoup   int          not null,
    piece    varchar(250) not null,
    coup     varchar(250) not null,
    constraint historique_chess_idPartie_fk
        foreign key (idPartie) references chess (idPartie)
);

create table plateau (
    idPartie int        not null,
    POSITION varchar(5) not null,
    piece    varchar(5) not null,
    constraint plateau_chess_idPartie_fk
        foreign key (idPartie) references chess (idPartie)
);

create table utilisateur (
    pseudo varchar(250) not null
        primary key,
    mdp    varchar(250) not null,
    uuid   varchar(250) not null
);
