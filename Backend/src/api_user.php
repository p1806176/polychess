<?php
require_once('sql.php');

function jsonError(string $error): string {
	return '{"success":false,"error":"' . $error . '"}';
}

function inscription(mysqli $link, string $username, string $password): string {
	if (!preg_match('/^\w*$/', $username)) {
		// Vérifie si $username ne contient des caractères autres que des lettres, chiffres ou _
		return jsonError("Username can only contains alphanumeric or '_' characters");
	} elseif (strlen($username) < 4) {
		return jsonError("Username must be at least 4 characters long");
	} elseif (strlen($password) < 4) {
		return jsonError("Password must be at least 4 characters long");
	} else if (strlen($username) > 20) {
		return jsonError("Username must be less than 20 characters long");
	} else if (strlen($password) > 20) {
		return jsonError("Password must be less than 20 characters long");
	} else {
		$result = createUtilisateur($link, $username, md5($password));

		if (!($result === true)) { // Si erreur
			return jsonError($result);
		} else {
			return '{"success":true}';
		}
	}
}

function connexion(mysqli $link, string $username, string $password): string {

	$query = mysqli_query($link, "SELECT uuid FROM utilisateur WHERE pseudo = '$username' AND mdp = '" . md5($password) . "'");
	if (mysqli_num_rows($query) == 0) {
		return jsonError("Wrong username or password");
	} else {
		return '{"success":true,"username":"' . $username . '","uuid":"' . $query->fetch_row()[0] . '"}';
	}
}

$json = null;

$action = isset($_POST['action']) ? $_POST['action'] : null;
$username = isset($_POST['username']) ? $_POST['username'] : null;
$password = isset($_POST['password']) ? $_POST['password'] : null;
$link = getConnection();

if ($action === null) {
	$json = jsonError("No action parameter");
} elseif ($username === null) {
	$json = jsonError("No username parameter");
} elseif ($password === null) {
	$json = jsonError("No password parameter");
} else {
	if ($action == "login") {
		$json = connexion($link, $username, $password);
	} elseif ($action == "register") {
		$json = inscription($link, $username, $password);
	} else {
		$json = jsonError("Invalid action '$action'");
	}
}

closeConnexion($link);

header('Content-Type: application/json');
echo $json . "\n";
