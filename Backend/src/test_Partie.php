<style type="text/css">
    body {
        font-family: monospace;
        font-size: 12pt;
    }

    td, th {
        width: 30px;
        height: 30px;
        text-align: center;
    }

    table, td {
        border: 2px groove black;
        border-collapse: collapse;
    }

    td {
        font-weight: bold;
    }

    .noir {
        background-color: black;
        color: #7EE;
    }

    .blanc {
        background-color: white;
        color: #3AA;
    }

    .null {
        background-color: lightgray;
    }
</style>

<?php

require_once('partie.php');

function afficherPlateau($plateau) {
	echo "<table>";
	for ($i = 7; $i >= 0; $i--) {
		echo "<th>".($i+1)."</th>";
		for ($j = 0; $j < 8; $j++) {
			$piece = $plateau[$j][$i];
			if ($piece == NULL) {
				echo "<td class='null'></td>";
			} else {
				echo '<td class="' . ($piece->getTeamPiece() == 0 ? 'noir' : 'blanc') . '">' . $piece->getTypePiece() . '</td>';
			}
		}
		echo "</tr>";
	}
	echo "<td></td>";
	for ($j = 'A'; $j < 'I'; $j++) {
		echo "<th>$j</th>";
	}
	echo "</table><br />";
}

//création nouvelle partie : 

//$partie1->initPartie();
//$partie1->creerPartieDansBDD();


$plateau = array();
for ($i = 0; $i < 8; $i++)
    $plateau[$i] = array(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
    
function ajoutePieceDansPlateau($piece, $plateau) {
    $plateau[$piece->getPosX()][$piece->getPosY()] = $piece;
    return $plateau;
}

// Test de la fonction enEchec

$roi_noir = new Piece("k", 4, 7, 0);
$tour_blanche = new piece("t", 7, 7, 1);

$plateau = ajoutePieceDansPlateau($roi_noir, $plateau);
$plateau = ajoutePieceDansPlateau($tour_blanche, $plateau);

$listePieces = array();
for ($i = 7; $i >= 0; $i--) {
	for ($j = 0; $j < 8; $j++) {
		if ($plateau[$j][$i] != NULL) {
			array_push($listePieces, $plateau[$j][$i]);
		}
	}
}

$partie1 = new partie('J1',1, getConnection());
$partie1->setPieces($listePieces);
//afficherPlateau($partie1->getPlateau());
if($partie1->enEchec(0) == true)
{
    echo "Succes : le roi noir est en echec.";
    echo "<br>";
}
else
{
    echo "Erreur : le roi noir n'est pas en echec.";
    echo "<br>";
}

// Test de la fonction checkFinPartie (echec et mat + pat)

$roi_blanc = new Piece("k", 4, 5, 1);
$plateau = ajoutePieceDansPlateau($roi_blanc, $plateau);

$listePieces = array();
for ($i = 7; $i >= 0; $i--) {
	for ($j = 0; $j < 8; $j++) {
		if ($plateau[$j][$i] != NULL) {
			array_push($listePieces, $plateau[$j][$i]);
		}
	}
}

$partie1 = new partie('J1',1, getConnection());
$partie1->setPieces($listePieces);
//afficherPlateau($partie1->getPlateau());

if($partie1->checkFinPartie() == true)
{
    echo "Succes : le roi noir est en echec et mat.";
    echo "<br>"; 
}
else
{
    echo "Erreur : le roi noir n'est pas en echec et mat.";
    echo "<br>";
}

// on reanitialise le plateau pour creer la situation de pat
for ($i = 0; $i < 8; $i++)
    $plateau[$i] = array(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

$roi_blanc = new Piece("k", 0, 7, 0);
$reine_noire = new piece("q", 1, 5, 1);

$plateau = ajoutePieceDansPlateau($roi_blanc, $plateau);
$plateau = ajoutePieceDansPlateau($reine_noire, $plateau);

for ($i = 7; $i >= 0; $i--) {
	for ($j = 0; $j < 8; $j++) {
		if ($plateau[$j][$i] != NULL) {
			array_push($listePieces, $plateau[$j][$i]);
		}
	}
}

$partie2 = new partie('J1',1, getConnection());
$partie2->setPieces($listePieces);
//afficherPlateau($partie1->getPlateau());
if($partie1->checkFinPartie() == true)
{
    echo "Succes : l'equipe blanche est pat.";
    echo "<br>"; 
}
else
{
    echo "Erreur : l'équipe blanche n'est pas pat.";
    echo "<br>";
}

//lecture partie :
//$partie1->chargerPartieDepuisBDD();
//$partie1->jouerCoup("P7674");
//$partie1->enregistrerCoupDansBDD("P7674");


afficherPlateau($partie1->getPlateau());
echo "nb coups : ".$partie1->getNbCoups();

echo "</br> renvoi partie n°3 (juste initialisée...) : </br>";
$partie3 = new partie('Joueur1-3',2, getConnection());
//$partie3->initPartie();
//$partie3->creerPartieDansBDD();
$partie3->chargerPartieDepuisBDD();
afficherPlateau($partie3->getPlateau());
echo $partie3-> renvoyerPartie();

?>