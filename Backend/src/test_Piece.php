<style type="text/css">
    body {
        font-family: monospace;
        font-size: 12pt;
    }

    td, th {
        width: 30px;
        height: 30px;
        text-align: center;
    }

    table, td {
        border: 2px groove black;
        border-collapse: collapse;
    }

    td {
        font-weight: bold;
    }

    .noir {
        background-color: black;
        color: #7EE;
    }

    .blanc {
        background-color: white;
        color: #3AA;
    }

    .null {
        background-color: lightgray;
    }
</style>

<?php

require_once('piece.php');
require_once('sql.php');

$plateau = array();
for ($i = 0; $i < 8; $i++)
	$plateau[$i] = array(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

function afficherPlateau($plateau) {
	echo "<table>";
	for ($i = 7; $i >= 0; $i--) {
		echo "<th>$i</th>";
		for ($j = 0; $j < 8; $j++) {
			$piece = $plateau[$j][$i];
			if ($piece == NULL) {
				echo "<td class='null'></td>";
			} else {
				echo '<td class="' . ($piece->getTeamPiece() == 0 ? 'noir' : 'blanc') . '">' . $piece->getTypePiece() . '</td>';
			}
		}
		echo "</tr>";
	}
	echo "<td></td>";
	for ($j = 'A'; $j < 'I'; $j++) {
		echo "<th>$j</th>";
	}
	echo "</table><br />";
}

function ajoutePieceDansPlateau($piece, $plateau) {
	$plateau[$piece->getPosX()][$piece->getPosY()] = $piece;
	return $plateau;
}

/**
 * Si $num est nombre entre 0 et 7, renvoie la lettre entre A et H correspondante.
 * Sinon, renvoie $num.
 */
function getLettreFromNum($num) {
	return (ord($num) >= ord("0") and ord($num) < ord("8")) ? chr(intval($num) + ord("A")) : $num;
}

function afficheListe($piece) {
	$listeCoup = $piece->getListeCoupsJouables();
	$i = 0;
	echo "Liste de coups de " . $piece->getTypePiece();
	if ($piece->getTeamPiece() == 0)
		echo "n";
	else
		echo "b";
	echo "[" . chr($piece->getPosX() + ord('A')) . "." . $piece->getPosY() . "] : ";
	while ($listeCoup[$i] != 'XX') {
		echo(getLettreFromNum($listeCoup[$i][0]) . $listeCoup[$i][1] . " - ");
		$i++;
	}
	echo($listeCoup[$i]); // Affiche 'XX' si tout va bien
	echo "<br>";
}

function afficheListeReduite($piece) {
	$listeCoup = $piece->getListeCoupsReduite();
	$i = 0;
	echo "Liste de coups de " . $piece->getTypePiece();
	if ($piece->getTeamPiece() == 0)
		echo "n";
	else
		echo "b";
	echo "[" . chr($piece->getPosX() + ord('A')) . "." . $piece->getPosY() . "] : ";
	while ($listeCoup[$i] != 'XX') {
		echo(getLettreFromNum($listeCoup[$i][0]) . $listeCoup[$i][1] . " - ");
		$i++;
	}
	echo($listeCoup[$i]); // Affiche 'XX' si tout va bien
	echo "<br>";
}

$pion_noir = new piece("p", 1, 2, 0);
$pion_blanc = new piece("p", 5, 1, 1); 
$fou_noir = new piece("b", 4, 1, 0);
$fou_blanc = new piece("b", 2, 3, 1);
$cavalier_blanc = new piece("n", 5, 5, 1);
$pion_blanc2 = new piece("p", 0, 1, 1);

$roi_blanc = new piece("k", 4, 0, 1);
$roi_noir = new piece("k", 7, 7, 0);
$tour_blanche = new piece("t", 7, 0, 1);
$reine_noir = new piece("q",3, 4, 0);

$pion_blanc3 = new piece("p", 7, 4, 1);
$pion_noir2 = new piece("p", 6, 6, 0);

// !!!!! UTILISER LA FONCTION AjouterPieceDansPlateau() !!!!!
/*
$plateau[0] = array($pion_blanc2, NULL, NULL, NULL, NULL, NULL, NULL, NULL); //Ligne X = 0
$plateau[1] = array(NULL, $pion_noir, NULL, NULL, NULL, NULL, NULL, NULL);//Ligne X = 1
$plateau[2] = array(NULL, NULL, $fou_blanc, NULL, NULL, NULL, NULL, NULL);
$plateau[3] = array(NULL, $fou_noir, NULL, NULL, NULL, NULL, NULL, NULL);
$plateau[4] = array(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
$plateau[5] = array(NULL, NULL, NULL, NULL, NULL, $cavalier_blanc, NULL, NULL);
$plateau[6] = array(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
$plateau[7] = array(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
*/

print_r($tour_blanche);
echo "<br /><br />";

$plateau = ajoutePieceDansPlateau($roi_blanc, $plateau);
$plateau = ajoutePieceDansPlateau($tour_blanche, $plateau);
$plateau = ajoutePieceDansPlateau($fou_blanc, $plateau);
$plateau = ajoutePieceDansPlateau($pion_blanc, $plateau);
$plateau = ajoutePieceDansPlateau($fou_noir, ajoutePieceDansPlateau($pion_noir, ajoutePieceDansPlateau($pion_blanc2, ajoutePieceDansPlateau($cavalier_blanc, $plateau))));
$plateau = ajoutePieceDansPlateau($reine_noir, ajoutePieceDansPlateau($roi_noir, $plateau));
$plateau = ajoutePieceDansPlateau($pion_blanc3, $plateau);
$plateau = ajoutePieceDansPlateau($pion_noir2, $plateau);

$listePieces = array();
for ($i = 7; $i >= 0; $i--) {
	for ($j = 0; $j < 8; $j++) {
		if ($plateau[$j][$i] != NULL) {
			array_push($listePieces, $plateau[$j][$i]);
		}
	}
}

$partie1 = new Partie('J1',1, getConnection());
$partie1->setPieces($listePieces);

//simulation dans l'historique :
/*
$partie1->enregistrerCoupDansBDD("p6664");

$partie1->setNbCoups(1);

$partie1->enregistrerCoupDansBDD("b7665");
$partie1->setNbCoups(2);
$partie1->enregistrerCoupDansBDD("n7665");
$partie1->setNbCoups(3);
$partie1->enregistrerCoupDansBDD("p6664");

$partie1->setNbCoups(4);
$partie1->enregistrerCoupDansBDD("q6636");
*/
/* PAS BESOIN, ÇA SE FAIT TOUT SEUL DANS Partie::setPieces
for ($i = 7; $i >= 0; $i--) {
	for ($j = 0; $j < 8; $j++) {
		if ($plateau[$j][$i] != NULL) {
			$plateau[$i][$j]->setPartie($partie1);
		}
	}
}
*/

//affichage plateau :
afficherPlateau($plateau);


//
// affichage mouvements possibles
//
afficheListe($pion_noir);
//afficheListeReduite($pion_noir);
afficheListe($pion_blanc);
afficheListe($pion_blanc2);
afficheListe($fou_noir);
afficheListe($fou_blanc);
afficheListe($cavalier_blanc);
afficheListe($roi_blanc);
afficheListeReduite($roi_blanc);
afficheListe($reine_noir);

print_r($partie1->enEchec(0));

echo "prise en passant du pion blanc sur noir :</br>";

// pour ajouter le coup dans l'historique
$partie1->jouerCoup("p6664");

afficherPlateau($partie1->getPlateau());
afficheListe($pion_blanc3);
echo "<br />";

// Test export JSON
echo $roi_blanc->toJSON();

?>
