<?php
require_once('partie.php');
require_once('piece.php');
require_once('sql.php');

function letterToNumber(string $letter): int {
	return ord($letter) - ord('a');
}

function decodeCoup(array $coupPHP, int $couleur): string {
	$coup = "";
	$coup .= strtolower($coupPHP['piece'][1]);
	if ($couleur == 0) {
		$coup = strtoupper($coup);
	}
	$coup .= letterToNumber($coupPHP['pos'][0]) . ($coupPHP['pos'][1] - 1);
	$coup .= letterToNumber($coupPHP['move'][0]) . ($coupPHP['move'][1] - 1);
	return $coup;
}

function getCouleur(mysqli $link, int $idPartie, string $player): int {
	if (executeQuery(
			$link,
			"SELECT EXISTS(SELECT * FROM chess WHERE idPartie = $idPartie AND joueur1 = '$player');"
		)->fetch_row()[0] == 1) {
		return 1; // Le joueur joue les blancs
	} elseif (executeQuery(
			$link,
			"SELECT EXISTS(SELECT * FROM chess WHERE idPartie = $idPartie AND joueur2 = '$player');"
		)->fetch_row()[0] == 1) {
		return 0; // Le joueur joue les noirs
	} else {
		return -1; // La partie n'éxiste pas
	}
}

function jsonPlateau(Partie $partie, $couleur) {
	return '{"success":true,"board":"' . $partie->renvoyerPartie() . '", "your_turn":' .
		($couleur == $partie->getTour() ? "true" : "false") . '}';
}

function jsonGame(mysqli $link, string $uuid, bool $all): string {
	$json = "[";

	$list = executeQuery($link, "SELECT idPartie, joueur2, nom FROM chess WHERE joueur1 = '$uuid'" . ($all ? "" : " AND gagnant IS NULL") . ";");
	while (($row = $list->fetch_row()) != null) {
		$opponentNameRow = executeQuery($link, "SELECT pseudo FROM utilisateur WHERE uuid = '$row[1]'")->fetch_row();
		$opponentName = $opponentNameRow === null ? $row[1] : $opponentNameRow[0];
		$json .= '{"id":' . $row[0] . ',"game_name":"' . $row[2] . '","opponent":"' . $opponentName . '"},';
	}

	// enlève la virgule à la fin
	$json = substr($json, 0, strlen($json) - 1);

	$json .= "]";
	return $json;
}

function jsonError($error) {
	return '{"success":false,"error":"' . $error . '"}';
}

$json = null;
$link = getConnection();

if (isset($_GET['action'])) {
	$action = $_GET['action'];

	$uuid_player = isset($_GET['uuid_player'])
		? $_GET['uuid_player']
		: null;
	$id_game = isset($_GET['id_game'])
		? $_GET['id_game']
		: null;

	if ($uuid_player === null) {
		$json = jsonError("uuid_player undefined");
	} elseif ($action == "games") {
		$json = jsonGame($link, $uuid_player, false);
	} elseif ($action == "games_all") {
		$json = jsonGame($link, $uuid_player, true);
	} elseif ($id_game === null) {
		$json = jsonError("id_game undefined");
	} else {
		$couleur = getCouleur($link, $id_game, $uuid_player);
		if ($couleur == -1) {
			$json = jsonError("This game doesn't exists");
		} else {
			$partie = new Partie($uuid_player, $id_game, $link);
			$partie->chargerPartieDepuisBDD();

			if ($action == "board") {
				$json = jsonPlateau($partie, $couleur);
			} elseif ($action == "moves") {
				$json = '{"success":true,"moves":' . $partie->renvoyerListeDeplacements() . '}';
			} elseif ($action == "history") {
				$json = '{"success":true,"moves":' . $partie->renvoyerHistorique() . '}';
			} else {
				$json = jsonError("Invalid action '$action'");
			}
		}
	}
} elseif (isset($_POST['action'])) {
	$action = $_POST['action'];

	$uuid_player = isset($_POST['uuid_player'])
		? $_POST['uuid_player']
		: null;
	$id_game = isset($_POST['id_game'])
		? $_POST['id_game']
		: null;

	if ($uuid_player === null) {
		$json = jsonError("uuid_player undefined");
	} elseif ($action == "create") {
		$game_name = isset($_POST['game_name'])
			? $_POST['game_name']
			: null;
		$difficulty = isset($_POST['difficulty_level'])
			? $_POST['difficulty_level']
			: null;

		if ($game_name === null) {
			$json = jsonError("game_name undefined");
		} elseif (strlen($game_name) > 20) {
			$json = jsonError("game_name must be less than 20 chars long");
		} elseif ($difficulty === null) {
			$json = jsonError("difficulty_level undefined");
		} elseif (!(in_array($difficulty, array(1, 2, 3, 4)))) {
			$json = jsonError("difficulty_level must be a number between 1 and 4");
		} else {

			$partie = new Partie($uuid_player, null, $link);
			$partie->initPartie();
			$partie->creerPartieDansBDD($game_name, $difficulty);
			$json = '{"success":true,"id":' . $partie->getIdPartie() . '}';
		}
	} elseif ($id_game === null) {
		$json = jsonError("id_game undefined");
	} else {
		$couleur = getCouleur($link, $id_game, $uuid_player);
		if ($couleur == -1) {
			$json = jsonError("This game doesn't exists");
		} else {
			$partie = new Partie($uuid_player, $id_game, $link);
			$partie->chargerPartieDepuisBDD();

			if ($action == "play") {

				if ($couleur != $partie->getTour()) {
					$json = jsonError("It's not your turn");
				} elseif (!isset($_POST['move'])) {
					$json = jsonError("No move was sent");
				} else {
					$coupPHP = json_decode($_POST['move'], true);
					if ($coupPHP == null) {
						$json = jsonError("Unable to parse JSON '" . $_POST['move'] . "'");
					} elseif ($partie->getPlateau()[ord($coupPHP['pos'][0]) - ord('a')][$coupPHP['pos'][1] - 1]->getTeam() != $couleur) {
						$json = jsonError("This piece is not yours");
					} else {
						$coup = decodeCoup($coupPHP, $couleur);
						$partie->jouerCoup($coup);
						$partie->enregistrerCoupDansBDD($coup);

						$json = jsonPlateau($partie, $couleur);
					}
				}
			} else {
				$json = jsonError("Invalid action '$action'");
			}
		}
	}
} else {
	$json = jsonError("No GET and no POST action was found");
}

header('Content-Type: application/json');
echo $json . "\n";

closeConnexion($link);
