<?php

/*Cette fonction prend en entrée l'identifiant de la machine hôte de la base de données,
les identifiants (login, mot de passe) d'un utilisateur autorisé 
sur la base de données contenant les tables pour le chat et renvoie une connexion
active sur cette base de donnée. Sinon, un message d'erreur est affiché.*/
/*
function getConnection() {
	$mysqli = new mysqli("localhost", "peip2", "peip2020", "echec");
	if ($mysqli->connect_errno) {
		echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	return $mysqli;
}
*/
function getConnection() {
	$mysqli = new mysqli("giust.ddns.net", "peip2", "peip2020", "echec");
	if ($mysqli->connect_errno) {
		echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	return $mysqli;
}


/*Cette fonction prend en entrée une connexion vers la base de données du chat ainsi
qu'une requête SQL SELECT et renvoie les résultats de la requête.
Si le résultat est faux, un message d'erreur est affiché*/

function executeQuery($link, $query) {
	return $link->query($query);
}


/*Cette fonction prend en entrée une connexion vers la base de données du chat ainsi
qu'une requête SQL INSERT/UPDATE/DELETE et ne renvoie rien si la mise à jour a fonctionné, sinon un 
message d'erreur est affiché.*/

function executeUpdate($link, $query) {
	if (!$link->query($query)) {
		throw new RuntimeException("Erreur lors de la requête '$query'\nErreur MySQLi: $link->error");
	}
}


/*Cette fonction ferme la connexion active $link passée en entrée*/

function closeConnexion($link) {
	$link->close();
}


function getJoueur($Joueur, $idPartie, $link) {
	return (executeQuery($link, "
select distinct Joueur1
from chess
where Joueur1 = $Joueur
  and idPartie = $idPartie
                                    ") == $Joueur)
		or
		(executeQuery($link, "
select distinct Joueur2
from chess
where Joueur2 = $Joueur
  and idPartie = $idPartie
                                    ") == $Joueur);
}

function createUtilisateur($link, $pseudo, $mdp) {

	$query = executeQuery($link, "SELECT pseudo from utilisateur WHERE pseudo='$pseudo';");
	if (mysqli_num_rows($query) > 0) { // On regarde si le résultat est vide ou pas
		return "Username already used";
	}

	/* Création d'un uuid aléatoire */
	$uuid = "";

	$randChars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789_";

	srand((double)microtime() * time());

	// Taille de 20 caractères
	for ($i = 0; $i < 20; $i++) {
		$uuid .= $randChars[rand() % strlen($randChars)];
	}
	/* Fin de la création de uuid */

	executeUpdate($link, "INSERT INTO utilisateur (pseudo, mdp, uuid) VALUES ('$pseudo', '$mdp', '$uuid');");
	return true;

}
