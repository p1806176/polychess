<?php

require_once("sql.php");
require_once("piece.php");

class Partie {
	private array $listePieceVivante; // tableau avec les piece encore sur le plateau
	private array $plateau; //tableau 2D avec les pieces a leur position présente
	private int $nbPiece;
	private $temps;
	private int $nbCoups;
	private int $tour;
	private ?mysqli $link;
	private ?string $joueur;
	private ?int $idPartie;
	private array $historique; // Un tableau contenant l'historique des coups de la partie du plus récent au plus ancient.

	public function __construct(?string $Joueur, ?int $idPartie, ?mysqli $link) {
		$this->joueur = $Joueur;
		$this->idPartie = $idPartie;

		$this->link = $link;

		$this->reset();
	}

	private function reset() {
		$this->plateau = array();
		for ($i = 0; $i < 8; $i++) {
			$this->plateau[$i] = array();
			for ($j = 0; $j < 8; $j++) {
				$this->plateau[$i][$j] = NULL;
			}
		}
		$this->nbCoups = 0;
		$this->historique = array();
	}

	/**
	 * Précondition: La partie éxiste dans la base de donnée.
	 */
	public function chargerPartieDepuisBDD(): void {
		$this->reset();

		$plateauSQL = executeQuery($this->link, "SELECT piece, position FROM plateau WHERE plateau.idPartie = " . $this->idPartie . ";");

		$pieces = array();

		while (($pieceSQL = $plateauSQL->fetch_row()) != NULL) {

			$i = (int)$pieceSQL[1][0]; // convertit caractère en entier
			$j = (int)$pieceSQL[1][1];

			$team = null;

			if (in_array($pieceSQL[0], array("P", "T", "N", "B", "Q", "K"))) {
				$team = 0;
			} else if (in_array($pieceSQL[0], array("p", "t", "n", "b", "q", "k"))) {
				$team = 1;
			} else {
				throw new RuntimeException("Type de pièce $pieceSQL[0] inconnu.");
			}

			array_push($pieces, new piece(strtolower($pieceSQL[0]), $i, $j, $team));
		}

		$this->setPieces($pieces);

		$this->temps = executeQuery($this->link,
			"SELECT temps
			FROM chess
			WHERE idPartie = " . $this->idPartie)->fetch_all()[0][0];

		$requeteHistorique = executeQuery($this->link, "SELECT piece, coup FROM historique WHERE idPartie = $this->idPartie ORDER BY idCoup DESC");
		while (($coup = $requeteHistorique->fetch_row()) != null) {
			array_push($this->historique, $coup[0] . $coup[1]);
		}

		$this->nbCoups = executeQuery($this->link,
			"SELECT count(coup)
				FROM historique
				Where idPartie=$this->idPartie")->fetch_all()[0][0];

		// Si le nombre de coup joué est pair, c'est aux blancs de jouer, si c'est impair, c'est aux noirs.
		$this->tour = ($this->nbCoups + 1) % 2;
	}

	public function initPartie(): void {
		//création plateau :
		$this->reset();

		$pieces = array(
			// BLANCS
			new piece("t", 0, 0, 1),
			new piece("b", 2, 0, 1),
			new piece("n", 1, 0, 1),
			new piece("q", 3, 0, 1),
			new piece("k", 4, 0, 1),
			new piece("b", 5, 0, 1),
			new piece("n", 6, 0, 1),
			new piece("t", 7, 0, 1),

			// NOIRS
			new Piece("t", 0, 7, 0),
			new Piece("n", 1, 7, 0),
			new Piece("b", 2, 7, 0),
			new Piece("q", 3, 7, 0),
			new Piece("k", 4, 7, 0),
			new Piece("b", 5, 7, 0),
			new Piece("n", 6, 7, 0),
			new Piece("t", 7, 7, 0)
		);

		// PIONS
		for ($i = 0; $i < 8; $i++) {
			array_push($pieces, new Piece("p", $i, 1, 1));
			array_push($pieces, new Piece("p", $i, 6, 0));
		}

		// Met à jour le plateau avec les pièces
		$this->setPieces($pieces);

		// Initialization des variables pour le début de la partie
		$this->temps = 0;
		$this->nbCoups = 0;
		$this->tour = 0;
		$this->historique = array();
	}

	/**
	 * Précondition: la fonction reset() à été appelée.
	 * Postconditions:
	 *   La liste des pièces vivantes est la liste passée en paramètre.
	 *   $this->plateau contient les pièces de la liste passée en paramètre au bon emplacement.
	 * @param array $pieces Les pièces à ajouter au plateau.
	 */
	public function setPieces(array $pieces): void {
		$this->nbPiece = 0;
		$this->listePieceVivante = $pieces;
		foreach ($pieces as $p) {
			$this->plateau[$p->getPosX()][$p->getPosY()] = $p;
			$this->nbPiece++;
		}

		foreach ($pieces as $p) {
			$p->setPartie($this);
		}
	}

	public function getIAByLevel(int $level): string {
		if ($level < 0 || $level > 4) {
			throw new RuntimeException("Niveau d'IA '$level' invalide");
		}
		return executeQuery($this->link, "SELECT uuid FROM utilisateur WHERE pseudo = 'IA_level_$level'")->fetch_row()[0];
	}

	public function creerPartieDansBDD(string $name, int $difficulty): void {
		$this->idPartie = executeQuery($this->link, "SELECT idPartie FROM chess ORDER BY idPartie DESC LIMIT 1;")->fetch_row()[0] + 1;

		//création partie dans chess
		$requeteInit = "
INSERT INTO chess (idPartie, joueur1, joueur2, temps, nbCoups, nom)
VALUES ($this->idPartie, '$this->joueur', '" . $this->getIAByLevel($difficulty) . "', 0, 0, '$name');
";
		executeUpdate($this->link, $requeteInit);

		// On insère dans la BDD la liste des pièces
		foreach ($this->listePieceVivante as $piece) {
			$typePiece = $piece->getTypePiece();

			if ($piece->getTeam() == 0) {
				$typePiece = strtoupper($typePiece);
			}

			executeUpdate($this->link, "INSERT INTO plateau (idPartie, position, piece)
VALUES ($this->idPartie, '" . $piece->getPosX() . $piece->getPosY() . "', '" . $typePiece . "');");
		}

	}

	public function jouerCoup(string $coup): void {
		$piece = strtolower(substr($coup, 0, 1));
		$depart = substr($coup, 1, 2);
		$arrivee = substr($coup, 3, 2);

		if ($this->plateau[$depart[0]][$depart[1]] == NULL) {
			throw new RuntimeException("Il n'y a pas de pièce à la position $depart.");
		}

		$team = $this->plateau[$depart[0]][$depart[1]]->getTeam();

		if (!in_array($arrivee, $this->plateau[$depart[0]][$depart[1]]->getListeCoupsJouables())) {
			throw new RuntimeException("Cette pièce ($piece$depart) ne peut pas effectuer ce mouvement ($depart->$arrivee)");
		}

		// On supprime la pièce mangée
		if ($this->plateau[$arrivee[0]][$arrivee[1]] != NULL) {
			$this->modifierPiece($arrivee[0], $arrivee[1], NULL);
		} else {
			if ($piece == "p" && $arrivee[0] != $depart[0]) {
				// Prise en passant
				if ($team == 1) {
					if ($this->plateau[$arrivee[0]][$arrivee[1] - 1] != NULL) {
						$this->modifierPiece($arrivee[0], $arrivee[1] - 1, NULL);
						executeUpdate($this->link, "DELETE FROM plateau WHERE idPartie=$this->idPartie AND position='" . $arrivee[0] . ($arrivee[1] - 1) . "'");
					}
				} else {
					if ($this->plateau[$arrivee[0]][$arrivee[1] + 1] != NULL) {
						$this->modifierPiece($arrivee[0], $arrivee[1] + 1, NULL);
						executeUpdate($this->link, "DELETE FROM plateau WHERE idPartie=$this->idPartie AND POSITION='" . $arrivee[0] . ($arrivee[1] + 1) . "'");
					}
				}
			}
		}

		if ($piece == "k" && $depart[0] == 4) {
			if ($arrivee[0] == 6) {
				// Petit Roque
				$this->modifierPiece(7, $depart[1], new Piece("t", 5, $depart[1], $team));
			}

			if ($arrivee[0] == 2) {
				// Grand Roque
				$this->modifierPiece(0, $depart[1], new Piece("t", 3, $depart[1], $team));
			}
		}

		// On déplace la pièce jouée
		$newPiece = new Piece($piece, (int)$arrivee[0], (int)$arrivee[1], $this->plateau[$depart[0]][$depart[1]]->getTeam());
		$this->modifierPiece($depart[0], $depart[1], $newPiece);

		// On ajoute le coup dans l'historique
		array_unshift($this->historique, $coup);
	}

	public function enregistrerCoupDansBDD(string $coup): void {
		$piece = substr($coup, 0, 1);
		$depart = substr($coup, 1, 2);
		$arrivee = substr($coup, 3, 2);

		// ROQUE
		if (strtolower($piece) == "k" && $depart[0] == 4) {
			if ($arrivee[0] == 6) {
				// Petit Roque
				executeUpdate($this->link, "UPDATE plateau SET POSITION='5" . $depart[1] . "' WHERE idPartie=$this->idPartie AND POSITION='7" . $depart[1] . "'");
			}

			if ($arrivee[0] == 2) {
				// Grand Roque
				executeUpdate($this->link, "UPDATE plateau SET POSITION='3" . $depart[1] . "' WHERE idPartie=$this->idPartie AND POSITION='0" . $depart[1] . "'");
			}
		}

		// Supprime la pièce qui se déplace à sa position de départ.
		executeUpdate($this->link, "DELETE FROM plateau WHERE idPartie = $this->idPartie AND position = $depart");
		// Supprime la pièce mangée. Si aucune pièce n'est mangée, ne fait rien.
		executeUpdate($this->link, "DELETE FROM plateau WHERE idPartie = $this->idPartie AND position = $arrivee");

		// Insère la pièce qui se déplace avec sa position d'arrivée.
		executeUpdate($this->link, "INSERT INTO plateau (idPartie, position, piece) VALUE ($this->idPartie, '$arrivee', '$piece')");

		// Ajoute le coup dans l'historique à la position 0 et on déplace tout le reste d'un rang.
		executeUpdate($this->link, "INSERT INTO historique (idPartie, idCoup, piece, coup) values ('$this->idPartie', $this->nbCoups, '$piece', '$depart$arrivee')");

		$this->nbCoups++;
		$this->tour = ($this->nbCoups + 1) % 2;
	}

	public function renvoyerPartie(): string {
		$res = "";

		//plateau
		for ($j = 7; $j >= 0; $j--) {
			for ($i = 0; $i < 8; $i++) {
				$piece = $this->plateau[$i][$j];
				if ($piece == null) {                // S'il n'y a pas de pièce.
					$res .= "NULL ";
				} else if ($piece->getTeam() == 0) {// Si la pièce est noire
					$res .= $piece->getTypePiece() . " ";
				} else {                            // Si la pièce est blanche
					$res .= strtoupper($piece->getTypePiece()) . " ";
				}
			}
			if ($j != 0) {
				$res .= '$ ';
			}
		}

		$res = str_replace("t", "r", $res);
		$res = str_replace("T", "R", $res);

		return $res;
	}

	public function renvoyerListeDeplacements(): string {
		$liste = "[";

		for ($i = $this->nbPiece - 1; $i > 0; $i--) {
			$liste .= $this->listePieceVivante[$i]->toJson() . ", ";
		}
		$liste .= $this->listePieceVivante[0]->toJSON();

		$liste .= "]";
		return $liste;
	}

	public function renvoyerHistorique(): string {
		$historique = "[";

		if ($this->historique != array()) {
			for ($i = $this->nbCoups - 1; $i > 0; $i--) {
				$historique .= '"' . $this->historique[$i] . '",';
			}
			$historique .= '"' . $this->historique[0] . '"';
		}

		$historique .= "]";
		return $historique;
	}

	public function getListePieceVivante(): array {
		return $this->listePieceVivante;
	}

	public function getNbPiece(): int {
		return $this->nbPiece;
	}

	public function traducteurCoord($c) {
		$a = intval('a');
		$c = intval($c);
		return chr($a + $c);
	}

	public function promotion($promotionType): void
		// Important : discuter de l'utilisation de cette fonction avec le groupe front-end.
		// $promotionType est le type de la piece choisi par l'utilisateur pour remplacer le pion ayant atteint la case finale.
		// Idee 1 : Dans le main, on met un "if(isset($promotionType))" puis on appel cette fonction qui va modifier le type de la piece.
		// Idee 2 : Si trop complique ou probleme avec front-end, on ne fera la promotion qu'en dame.
	{
		for ($j = 0; $j < 8; $j++) {
			if ($this->plateau[0][$j]->typePiece == 'P') {
				$this->plateau[0][$j]->setTypePiece($promotionType);
			}

			if ($this->plateau[7][$j]->typePiece == 'p') {
				$this->plateau[7][$j]->setTypePiece($promotionType);
			}
		}
	}

	private function trouverRoi(int $team): int {
		for ($i = 0; $i < $this->nbPiece; $i++) {
			if ($this->listePieceVivante[$i]->getTypePiece() == 'k' and $this->listePieceVivante[$i]->getTeam() == $team) {
				return $i;
			}
		}

		throw new RuntimeException("Le roi n'est pas sur le plateau, OÙ EST-IL ?!");
	}

	public function enEchec(int $team): bool {
		$roi = $this->listePieceVivante[$this->trouverRoi($team)];

		for ($i = 0; $i < $this->nbPiece; $i++) {
			$piece = $this->listePieceVivante[$i];
			if ($piece->getTeam() != $team) {
				if (in_array($roi->getPosX() . $roi->getPosY(), $piece->getListeCoupsJouables())) {
					return true;
				}
			}
		}

		return false;
	}

	public function estMat(int $team): bool {
		if ($this->enEchec($team)) {
			for ($i = 0; $i < $this->nbPiece; $i++) {
				$piece = $this->listePieceVivante[$i];
				if ($piece->getTeam() == $team) {
					if ($piece->getListeCoupsReduite() != array("XX")) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public function estPat(int $team): bool {
		if (!$this->enEchec($team)) {
			for ($i = 0; $i < $this->nbPiece; $i++) {
				$piece = $this->listePieceVivante[$i];
				if ($piece->getTeam() == $team) {
					if ($piece->getListeCoupsReduite() != array("XX")) {
						return false;
					}
				}
			}
		}
		return false;
	}

	// fonction qui verifie l'issue du jeu en renvoyant true si la partie est finie ou
	// false si la partie est toujours en cours.
	public function checkFinPartie(): bool {
		return $this->estMat(0) || $this->estMat(1) || $this->estPat(0) || $this->estPat(1);
	}

	/*
	 * 0 = noir
	 * 1 = blanc
	 * 2 = nul
	 */
	public function setGagnantDansBDD(int $gagnant) {
		executeUpdate($this->link, "UPDATE chess SET gagnant=$gagnant WHERE idPartie = $this->idPartie;");
	}

	public function getPlateau(): array {
		return $this->plateau;
	}

	public function getHistorique(): array {
		return $this->historique;
	}

	/*
	 * Précondition: Il existe bien une pièce à la position (posX; posY)
	 * Supprime la pièce si NULL est passé en paramètre.
	 */
	public function modifierPiece(int $posX, int $posY, ?Piece $nouvellePiece): void {
		$index = -1;
		for ($i = 0; $i < $this->nbPiece && $index == -1; $i++) {
			$p = $this->listePieceVivante[$i];
			if ($p->getPosX() == $posX && $p->getPosY() == $posY) {
				$index = $i;
			}
		}

		// Enlève la pièce si $nouvellePiece = NULL, copie nouvelle pièce.
		if ($nouvellePiece == NULL) {
			// On enlève la pièce et on replace les autres pièces dans la liste pour boucher le trou.
			array_splice($this->listePieceVivante, $index, 1);
			$this->nbPiece--;
		} else {
			$this->listePieceVivante[$index]->setCopy($nouvellePiece);
		}

		// Enlève la piece du plateau, la met dans le nouvel emplacement si $nouvellePiece != NULL
		$this->plateau[$posX][$posY] = NULL;
		if ($nouvellePiece != NULL) {
			$this->plateau[$nouvellePiece->getPosX()][$nouvellePiece->getPosY()] = $this->listePieceVivante[$index];
		}
	}

	public function getCopy(): Partie {
		$copie = new Partie(null, null, null);
		$copieListePieces = array();
		foreach ($this->listePieceVivante as $piece) {
			array_push($copieListePieces, $piece->getCopy());
		}
		$copie->setPieces($copieListePieces);

		$copie->historique = array();
		for ($i = 0; $i < $this->nbCoups; $i++) {
			$copie->historique[$i] = $this->historique[$i];
		}

		return $copie;
	}

	public function getTemps() {
		return $this->temps;
	}

	public function getNbCoups(): int {
		return $this->nbCoups;
	}

	public function getTour(): int {
		return $this->tour;
	}

	public function getJoueur(): ?string {
		return $this->joueur;
	}

	public function getIdPartie(): ?int {
		return $this->idPartie;
	}

}
