<?php

require_once('sql.php');
require_once('partie.php');

class Piece {
	private int $posX;//de 0 à 7
	private int $posY;//de 0 à 7
	private string $typePiece;//caractère selon piece
	private int $team;//(noir/blanc) un nombre 0/1
	private ?Partie $partie; // instance de la partie;

	public function __construct(string $typePiece, int $posX, int $posY, int $team) {
		$this->setTypePiece($typePiece);
		$this->setPosX($posX);
		$this->setPosY($posY);
		$this->setTeam($team);
		$this->partie = null;
	}

	public function getPosX(): int {
		return $this->posX;
	}

	public function setPosX(int $posX): void {
		if (($posX > 7) or ($posX < 0)) {
			throw new InvalidArgumentException("La position '$posX' n'est pas une position valide");
		}

		$this->posX = $posX;
	}

	public function getPosY(): int {
		return $this->posY;
	}

	public function setPosY(int $posY): void {
		if (($posY > 7) or ($posY < 0)) {
			throw new InvalidArgumentException("\"La position '$posY' n'est pas une position valide\"");
		}

		$this->posY = $posY;
	}

	public function getTypePiece(): string {
		return $this->typePiece;
	}

	public function setTypePiece(string $typePiece): void {
		if (!in_array($typePiece, array("p", "b", "n", "q", "k", "t"))) {
			throw new InvalidArgumentException("Type de pièce '$typePiece' invalide.");
		}

		$this->typePiece = $typePiece;
	}

	public function getTeamPiece(): int {
		return $this->team;
	}

	public function toJSONNoMove(): string {
		return '{"type": "' . $this->typePiece . '","team": "' . ($this->team == 0 ? "black" : "white") . '","pos": {"x": ' . $this->posX . ', "y": ' . $this->posY . '}}';
	}

	public function toJSON(): string {
		return '{
			"piece": "' . ($this->team == 0 ? "b" : "w") . strtoupper($this->typePiece) . '",
			"pos": "' . $this->numberToLetter($this->posX) . ($this->posY + 1) . '",
			"moves" : [' . $this->movesToJSON() . ']
		}';
	}

	private function movesToJSON(): string {
		$moves = $this->getListeCoupsReduite();
		if ($moves[0] != "XX") {
			$ret = '"' . $this->numberToLetter($moves[0][0]) . ($moves[0][1] + 1) . '"';
			for ($i = 1; $moves[$i] != "XX"; $i++) {
				$ret .= ', "' . $this->numberToLetter($moves[$i][0]) . ($moves[$i][1] + 1) . '"';
			}
			return $ret;
		} else {
			return "";
		}
	}

	public function getListeCoupsJouables(): array {
		//création du tableau contenant la liste de coups
		$listeCoups = array();

		//en fonction du type de la piece : pion,tour,..
		switch ($this->typePiece) {
			case 'p' : //c un PION
				// NORMALEMENT pas besoin de vérifier si il est au bout du tableau car s'il y arrive alors il est promu.
				// liste des coups possibles pour le pion blanc (équipe 1) :
				if ($this->team == 1) {
					if ($this->caseVide($this->posX, $this->posY + 1)) {
						array_push($listeCoups, $this->posX . ($this->posY + 1));
						if ($this->posY == 1 && $this->caseVide($this->posX, $this->posY + 2)) {
							array_push($listeCoups, $this->posX . ($this->posY + 2));
						}
					}

					//la prise en "croisée" : en cours ...
					//prise diagonale droite : ↗
					if ($this->caseContientEnnemi($this->posX + 1, $this->posY + 1)) {
						array_push($listeCoups, ($this->posX + 1) . ($this->posY + 1));
					}

					//prise diagonale gauche : ↖
					if ($this->caseContientEnnemi($this->posX - 1, $this->posY + 1)) { // vérifie que l'on n'est pas sur le bord gauche du plateau et que la case devant à gauche contient une piece
						array_push($listeCoups, ($this->posX - 1) . ($this->posY + 1));
					}

					// EN PASSANT
//					$caseCote = $this->posX + 1; // coté droit : ↗
//					$caseCote = $this->posX - 1; // coté gauche : ↖
//					On teste les deux cas dans le for pour éviter les doublons de code.

					for ($caseCote = $this->posX - 1; $caseCote < $this->posX + 2; $caseCote += 2) {
						if ($this->caseContientEnnemi($caseCote, $this->posY)) {
							if ($this->partie->getplateau()[$caseCote][$this->posY]->getTypePiece() == 'p') { // si l'ennemi ciblé est un pion

								// Si le dernier coup est un saut de deux case de la part du pion a coté
								if ($coupAdverse = $this->partie->getHistorique()[0] == "P" . $caseCote . "6" . $caseCote . "4") {
									array_push($listeCoups, ($caseCote . ($this->posY + 1)));
								}
							}
						}
					}

				} //liste des coups possibles pour le pion noir (équipe 0) :
				else if ($this->team == 0) {
					if ($this->caseVide($this->posX, $this->posY - 1)) {
						array_push($listeCoups, $this->posX . ($this->posY - 1));

						if ($this->posY == 6 && $this->caseVide($this->posX, $this->posY - 2)) {
							array_push($listeCoups, $this->posX . ($this->posY - 2));
						}
					}

					//la prise en "croisée" : en cours ...
					//prise diagonale gauche (noir) : ↙
					if ($this->caseContientEnnemi($this->posX - 1, $this->posY - 1)) { // vérifie que l'on n'est pas sur le bord gauche du plateau et que la case sous lui à gauche contient une piece
						array_push($listeCoups, ($this->posX - 1) . ($this->posY - 1));
					}
					//prise diagonale droite (noir) : ↘
					if ($this->caseContientEnnemi($this->posX + 1, $this->posY - 1)) { // vérifie que l'on n'est pas sur le bord droit du plateau et que la case devant à gauche contient une piece
						array_push($listeCoups, ($this->posX + 1) . ($this->posY - 1));
					}

					// EN PASSANT
//					$caseCote = $this->posX + 1; // coté droit ↘:
//					$caseCote = $this->posX - 1; // coté gauche ↙:
//					On teste les deux cas dans le for pour éviter les doublons de code.

					for ($caseCote = $this->posX - 1; $caseCote < $this->posX + 2; $caseCote += 2) {
						if ($this->caseContientEnnemi($caseCote, $this->posY)) {
							if ($this->partie->getplateau()[$caseCote][$this->posY]->getTypePiece() == 'p') { // si l'ennemi ciblé est un pion

								// Si le dernier coup est un saut de deux case de la part du pion a coté
								if ($this->partie->getHistorique()[0] == "p" . $caseCote . "1" . $caseCote . "3") {
									array_push($listeCoups, ($caseCote . ($this->posY - 1)));
								}
							}
						}
					}
				}

				//chaine de caractères de fin :
				array_push($listeCoups, "XX");
				break;

			case 'b' : // c un FOU
				//liste des coups possibles pour le fou (COMME MOI)

				//vérification liste avec affichage : en cours


				//première partie : cases (posX+1,posY+1) càd direction ↗
				$tempX = $this->posX;
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($tempX + 1, $tempY + 1)) { //tant que l'on ne sort pas du plateau et qu'il n'y a pas de pièce alliée dans la prochaine case :
					$tempX++;
					$tempY++;
					array_push($listeCoups, ($tempX) . ($tempY));
					if ($this->caseContientEnnemi($tempX, $tempY)) {
						$tempX = 8; // on sort de la boucle
					}
				}

				//deuxième partie : cases (posX+1,posY-1) càd direction ↘
				$tempX = $this->posX;
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($tempX + 1, $tempY - 1)) { //tant que l'on ne sort pas du plateau et qu'il n'y à rien dans la prochaine case :
					$tempX++;
					$tempY--;
					array_push($listeCoups, ($tempX) . ($tempY));

					if ($this->caseContientEnnemi($tempX, $tempY)) { //si dans case cible c un ennemi on ne peut pas aller plus loin :
						$tempX = 8; //sort de la boucle
					}
				}

				//troisème partie : cases (posX-1,posY-1) càd direction ↙
				$tempX = $this->posX;
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($tempX - 1, $tempY - 1)) { //tant que l'on ne sort pas du plateau et qu'il n'y à rien dans la prochaine case :
					$tempX--;
					$tempY--;
					array_push($listeCoups, ($tempX) . ($tempY));

					if ($this->caseContientEnnemi($tempX, $tempY)) { //si dans case cible c un ennemi on ne peut pas aller plus loin :
						$tempX = 0; //sort de la boucle
					}
				}

				//quatrième partie : cases (posX-1,posY-1) càd direction ↖
				$tempX = $this->posX;
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($tempX - 1, $tempY + 1)) { //tant que l'on ne sort pas du plateau et qu'il n'y à rien dans la prochaine case :
					$tempX--;
					$tempY++;
					array_push($listeCoups, ($tempX) . ($tempY));

					if ($this->caseContientEnnemi($tempX, $tempY)) { //si dans case cible c un ennemi on ne peut pas aller plus loin :
						$tempX = 0; //sort de la boucle
					}
				}

				//chaine de caractères de fin :
				array_push($listeCoups, 'XX');
				break;

			case 't' : // c une TOUR
				//liste des coups possibles pour la tour

				//1er partie déplacement horizontal gauche-droite : ->
				$tempX = $this->posX;
				while ($this->peutAllerDansCase($tempX + 1, $this->posY)) {
					$tempX++;
					array_push($listeCoups, ($tempX) . ($this->posY));

					if ($this->caseContientEnnemi($tempX, $this->posY)) {
						$tempX = 8;
					}
				}

				//2e partie déplacement horizontal droite-gauche : <-
				$tempX = $this->posX;
				while ($this->peutAllerDansCase($tempX - 1, $this->posY)) {
					$tempX--;
					array_push($listeCoups, ($tempX) . ($this->posY));
					if ($this->caseContientEnnemi($tempX, $this->posY)) {
						$tempX = 0;
					}
				}

				//3e partie déplacement horizontal bas-haut : ↑
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($this->posX, $tempY + 1)) {
					$tempY++;
					array_push($listeCoups, ($this->posX) . ($tempY));

					if ($this->caseContientEnnemi($this->posX, $tempY)) {
						$tempY = 8;
					}
				}

				//4e partie déplacement horizontal haut-bas : ↓
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($this->posX, $tempY - 1)) {
					$tempY--;
					array_push($listeCoups, ($this->posX) . ($tempY));

					if ($this->caseContientEnnemi($this->posX, $tempY)) {
						$tempY = 0;
					}
				}

				//cas possible avec le roque (petit et grand)
				// C'EST FAIT DANS LE CASE 'k'

				array_push($listeCoups, 'XX');
				break;

			case 'n' : // c un CAVALIER
				//liste des coups possibles pour le cavalier

				//1er partie déplacement droite-droite-haut : ->->↑
				$tempX = $this->posX + 2;
				$tempY = $this->posY + 1;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
				}

				//2eme partie déplacement droite-haut-haut : ->↑↑
				$tempX = $this->posX + 1;
				$tempY = $this->posY + 2;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
				}

				//3eme partie déplacement gauche-gauche-haut : <-<-↑
				$tempX = $this->posX - 2;
				$tempY = $this->posY + 1;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
				}

				//4eme partie déplacement gauche-haut-haut : <-↑↑
				$tempX = $this->posX - 1;
				$tempY = $this->posY + 2;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
				}

				//5eme partie déplacement gauche-gauche-bas : <-<-↓
				$tempX = $this->posX - 2;
				$tempY = $this->posY - 1;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
				}

				//6eme partie déplacement gauche-bas-bas : <-↓↓
				$tempX = $this->posX - 1;
				$tempY = $this->posY - 2;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
				}

				//7eme partie déplacement droite-bas-bas : ->↓↓
				$tempX = $this->posX + 1;
				$tempY = $this->posY - 2;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
				}

				//8eme partie déplacement droite-droite-bas : ->->↓
				$tempX = $this->posX + 2;
				$tempY = $this->posY - 1;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
				}

				array_push($listeCoups, 'XX');
				break;

			case 'q' : //c une REINE
				//liste des coups possibles pour la reine (tour + fou)

				//vérification liste avec affichage : en cours

				//1er partie déplacement horizontal gauche-droite : ->
				$tempX = $this->posX;
				while ($this->peutAllerDansCase($tempX + 1, $this->posY)) {
					$tempX++;
					array_push($listeCoups, ($tempX) . ($this->posY));

					if ($this->caseContientEnnemi($tempX, $this->posY)) {
						$tempX = 8;
					}
				}

				//2e partie déplacement horizontal droite-gauche : <-
				$tempX = $this->posX;
				while ($this->peutAllerDansCase($tempX - 1, $this->posY)) {
					$tempX--;
					array_push($listeCoups, ($tempX) . ($this->posY));

					if ($this->caseContientEnnemi($tempX, $this->posY)) {
						$tempX = 0;
					}
				}

				//3e partie déplacement horizontal bas-haut : ↑
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($this->posX, $tempY + 1)) {
					$tempY++;
					array_push($listeCoups, ($this->posX) . ($tempY));

					if ($this->caseContientEnnemi($this->posX, $tempY)) {
						$tempY = 8;
					}
				}

				//4e partie déplacement horizontal haut-bas : ↓
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($this->posX, $tempY - 1)) {
					$tempY--;
					array_push($listeCoups, ($this->posX) . ($tempY));

					if ($this->caseContientEnnemi($this->posX, $tempY)) {
						$tempY = 0;
					}
				}

				//5e partie : cases (posX+1,posY+1) càd direction ↗
				$tempX = $this->posX;
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($tempX + 1, $tempY + 1)) { //tant que l'on ne sort pas du plateau et qu'il n'y a pas de pièce alliée dans la prochaine case :
					$tempX++;
					$tempY++;
					array_push($listeCoups, ($tempX) . ($tempY));
					if ($this->caseContientEnnemi($tempX, $tempY)) {
						$tempX = 8; // on sort de la boucle
					}
				}

				//6e partie : cases (posX+1,posY-1) càd direction ↘
				$tempX = $this->posX;
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($tempX + 1, $tempY - 1)) { //tant que l'on ne sort pas du plateau et qu'il n'y à rien dans la prochaine case :
					$tempX++;
					$tempY--;
					array_push($listeCoups, ($tempX) . ($tempY));

					if ($this->caseContientEnnemi($tempX, $tempY)) { //si dans case cible c un ennemi on ne peut pas aller plus loin :
						$tempX = 8; //sort de la boucle
					}
				}

				//7e partie : cases (posX-1,posY-1) càd direction ↙
				$tempX = $this->posX;
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($tempX - 1, $tempY - 1)) { //tant que l'on ne sort pas du plateau et qu'il n'y à rien dans la prochaine case :
					$tempX--;
					$tempY--;
					array_push($listeCoups, ($tempX) . ($tempY));

					if ($this->caseContientEnnemi($tempX, $tempY)) { //si dans case cible c un ennemi on ne peut pas aller plus loin :
						$tempX = 0; //sort de la boucle
					}
				}

				//8e partie : cases (posX-1,posY-1) càd direction ↖
				$tempX = $this->posX;
				$tempY = $this->posY;
				while ($this->peutAllerDansCase($tempX - 1, $tempY + 1)) { //tant que l'on ne sort pas du plateau et qu'il n'y à rien dans la prochaine case :
					$tempX--;
					$tempY++;
					array_push($listeCoups, ($tempX) . ($tempY));

					if ($this->caseContientEnnemi($tempX, $tempY)) { //si dans case cible c un ennemi on ne peut pas aller plus loin :
						$tempX = 0; //sort de la boucle
					}
				}


				array_push($listeCoups, "XX");
				break;

			case 'k' : // c un ROI
				//liste des coups possibles pour le roi

				//1er partie déplacement gauche-droite : ->
				$tempX = $this->posX + 1;
				if ($this->peutAllerDansCase($tempX, $this->posY)) {
					array_push($listeCoups, ($tempX) . ($this->posY));
					//echo "->";
				}

				//2eme partie déplacement diagonale (+1,+1) : ↗
				$tempX = $this->posX + 1;
				$tempY = $this->posY + 1;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
					//echo "↗";
				}

				//3eme partie déplacement bas-haut : ↑
				$tempY = $this->posY + 1;
				if ($this->peutAllerDansCase($this->posX, $tempY)) {
					array_push($listeCoups, ($this->posX) . ($tempY));
					//echo "↑";
				}

				//4eme partie déplacement diagonale (-1,+1) : ↖
				$tempX = $this->posX - 1;
				$tempY = $this->posY + 1;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
					//echo "↖";
				}

				//5eme partie déplacement droite-gauche : <-
				$tempX = $this->posX - 1;
				if ($this->peutAllerDansCase($tempX, $this->posY)) {
					array_push($listeCoups, ($tempX) . ($this->posY));
					//echo "<-";
				}

				//6eme partie déplacement diagonale (-1,-1) : ↙
				$tempX = $this->posX - 1;
				$tempY = $this->posY - 1;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
					//echo "↙";
				}

				//7eme partie déplacement haut-bas : ↓
				$tempY = $this->posY - 1;
				if ($this->peutAllerDansCase($this->posX, $tempY)) {
					array_push($listeCoups, ($this->posX) . ($tempY));
					//echo "↓";
				}

				//8eme partie déplacement diagonale (+1,-1) : ↘
				$tempX = $this->posX + 1;
				$tempY = $this->posY - 1;
				if ($this->peutAllerDansCase($tempX, $tempY)) {
					array_push($listeCoups, ($tempX) . ($tempY));
					//echo "↘";
				}

				// ROQUE
				// reste à vérifier si le roi ou la tour à bougé...
				//proposition : requête SQL qui vérifie si tour ou roi en questions apparaissent dans la table historique (pour cela il faudra enregistrer les pièces dans la BD sous le format : "type équipe numéro")

				// le grand
				if ($this->posX == 4 && ($this->posY == 0 || $this->posY == 7)) { // il est au bon endroit
					$y = $this->posY;
					if ($this->caseContientAllie(0, $y) && $this->partie->getPlateau()[0][$y]->typePiece == 't') { // la tour est au bon endroit
						if ($this->caseVide(1, $y) && $this->caseVide(2, $y) && $this->caseVide(3, $y)) {
							array_push($listeCoups, "2$y");
						}
					}
				}


				// le petit
				if ($this->posX == 4 && ($this->posY == 0 || $this->posY == 7)) { // il est au bon enroit
					$y = $this->posY;
					if ($this->caseContientAllie(7, $y) && $this->partie->getPlateau()[7][$y]->typePiece == 't') { // la tour est au bon endroit
						if ($this->caseVide(6, $y) && $this->caseVide(5, $y)) {
							array_push($listeCoups, "6$y");
						}
					}
				}

				array_push($listeCoups, 'XX');
				break;

			default :
				throw new RuntimeException("Type de pièce inconnu: '$this->typePiece'");
		}

		return $listeCoups;
	}

	public function getListeCoupsReduite() {
		$listeCoups = $this->getListeCoupsJouables();

		$listeCoupsReduite = array();

		for ($i = 0; $listeCoups[$i] != "XX"; $i++) {
			$partie = $this->partie->getCopy();

			$partie->jouerCoup($this->typePiece . $this->posX . $this->posY . $listeCoups[$i]);

			if (!$partie->enEchec($this->team)) {
				array_push($listeCoupsReduite, $listeCoups[$i]);
			}
		}

		array_push($listeCoupsReduite, "XX");

		return $listeCoupsReduite;
	}

	public function getTeam(): int {
		return $this->team;
	}

	public function setTeam(int $team): void {
		if (!($team == 0 or $team == 1)) {
			throw new InvalidArgumentException("L'équipe doit être 0 ou 1");
		}

		$this->team = $team;
	}

	public function caseDansPlateau(int $x, int $y): bool {
		return $x >= 0 && $x < 8 && $y >= 0 && $y < 8;
	}

	public function peutAllerDansCase(int $x, int $y): bool {
		return $this->caseDansPlateau($x, $y) && !$this->caseContientAllie($x, $y);
	}

	private function caseVide(int $x, int $y): bool {
		return $this->caseDansPlateau($x, $y) && $this->partie->getPlateau()[$x][$y] == null;
	}

	private function caseContientEnnemi(int $x, int $y): bool {
		return $this->caseDansPlateau($x, $y) && !$this->caseVide($x, $y) && $this->partie->getPlateau()[$x][$y]->getTeam() != $this->getTeam();
	}

	private function caseContientAllie(int $x, int $y): bool {
		return $this->caseDansPlateau($x, $y) && !$this->caseVide($x, $y) && $this->partie->getPlateau()[$x][$y]->getTeam() == $this->getTeam();
	}

	public function setPartie(?Partie $partie): void {
		$this->partie = $partie;
	}

	public function getPartie(): ?Partie {
		return $this->partie;
	}

	public function setCopy(Piece $piece): void {
		$this->posX = $piece->posX;
		$this->posY = $piece->posY;
		$this->typePiece = $piece->typePiece;
	}

	public function getCopy(): Piece {
		return new Piece($this->typePiece, $this->posX, $this->posY, $this->team);
	}

	private function numberToLetter(int $num): string {
		return chr($num + ord('a'));
	}

}
