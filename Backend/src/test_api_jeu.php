<style type="text/css">
	body {
		font-family: monospace;
		font-size: 12pt;
	}

	td, th {
		width: 30px;
		height: 30px;
		text-align: center;
	}

	table, td {
		border: 2px groove black;
		border-collapse: collapse;
	}

	td {
		font-weight: bold;
	}

	.noir {
		background-color: black;
		color: #7EE;
	}

	.blanc {
		background-color: white;
		color: #3AA;
	}

	.null {
		background-color: lightgray;
	}
</style>

<?php

require_once('partie.php');

function afficherPlateau($plateau) {
	echo "<table>";
	for ($i = 7; $i >= 0; $i--) {
		echo "<th>" . ($i + 1) . "</th>";
		for ($j = 0; $j < 8; $j++) {
			$piece = $plateau[$j][$i];
			if ($piece == NULL) {
				echo "<td class='null'></td>";
			} else {
				echo '<td class="' . ($piece->getTeamPiece() == 0 ? 'noir' : 'blanc') . '">' . $piece->getTypePiece() . '</td>';
			}
		}
		echo "</tr>";
	}
	echo "<td></td>";
	for ($j = 'A'; $j < 'I'; $j++) {
		echo "<th>$j</th>";
	}
	echo "</table><br />";
}

function afficheListe($piece) {
	$listeCoup = $piece->getListeCoupsJouables();

	echo "Liste de coups de " . $piece->getTypePiece();

	if ($piece->getTeamPiece() == 0)
		echo "n";
	else
		echo "b";

	echo "[" . chr($piece->getPosX() + ord('A')) . "." . ($piece->getPosY() + 1) . "] : ";

	for ($i = 0; $listeCoup[$i] != 'XX'; $i++) {
		echo(chr($listeCoup[$i][0] + ord('A')) . ($listeCoup[$i][1] + 1) . " - ");
	}

	echo($listeCoup[$i]); // Affiche 'XX' si tout va bien
	echo "<br>";
}

function afficheListeReduite($piece) {
	$listeCoup = $piece->getListeCoupsReduite();

	echo "Liste de coups de " . $piece->getTypePiece();

	if ($piece->getTeamPiece() == 0)
		echo "n";
	else
		echo "b";

	echo "[" . chr($piece->getPosX() + ord('A')) . "." . ($piece->getPosY() + 1) . "] : ";

	for ($i = 0; $listeCoup[$i] != 'XX'; $i++) {
		echo(chr($listeCoup[$i][0] + ord('A')) . ($listeCoup[$i][1] + 1) . " - ");
	}

	echo($listeCoup[$i]); // Affiche 'XX' si tout va bien
	echo "<br>";
}

$link = getConnection();

$partie = new Partie("J4", 6, getConnection());
$partie->chargerPartieDepuisBDD();

afficherPlateau($partie->getPlateau());

for ($i = 0; $i < $partie->getNbPiece(); $i++) {
	afficheListe($partie->getListePieceVivante()[$i]);
}