# Backend Échecs

Pour les deux APIs, si la requête a réussi, la réponse est un objet JSON contenant `success = true` et les données demandées.
Si la requête a échouée, la réponse contient `success = false` et un message explicant l'érreur.

## API de connexion

Utilisable en envoyant une requête HTTP au fichier api_user.php

### Paramètres obligatoires :
* `action` : L'action à effectuer
* `username` : Le nom d'utilisateur
* `password` : Le mot de passe

### Les différentes actions :
* `action=login` Pour se connecter.<br>
  Si la connexion est réussie, renvoie le nom d'utilisateur et l'uuid du joueur.<br>
  Exemple de réponse si la connexion est réussie :

      {
        "success": true,
        "username": "username",
        "uuid": "sdGFesfESFEs0255fsdE"
      }
  Si la connexion a échouée :

      {
        "success": false,
        "error": "Wrong username or password",
      }
* `action=register` Pour s'inscrire, ne renvoie aucune donnée.
Si l'inscription a réussi :

      {
        "success": true
      }

## API de jeu

Utilisable en envoyant une requête HTTP au fichier api_game.php

### Requêtes `GET`

#### Paramètres obligatoires pour toutes les requêtes :
* `action` : L'action que l'on veut effectuer
* `id_game` : L'identifiant de la partie
* `uuid_player` : L'uuid du joueur renvoyé par l'API de connexion

#### Les différentes actions

* `action=board` : Renvoie le plateau du jeu.
* `action=moves` : Renvoie la liste des déplacements possibles pour toutes les pièces.
* `action=history`: Renvoie l'historique des coups de la partie du premier joué au dernier joué sous la forme
* `action=games` : Renvoie la liste des parties en cours du joueur sous la forme

      [
        {"id": 1, "game_name": "nom_de_la_partie", "opponent": "nom_de_l_adversaire"},
        {"id": 4, "game_name": "nom_de_la_partie", "opponent": "nom_de_l_adversaire"},
        {"id": 6, "game_name": "nom_de_la_partie", "opponent": "nom_de_l_adversaire"}
      ]
  
* `action=games_all` : Renvoie la liste de toutes les parties en cours du joueur sous le même format.

### Requêtes `POST`

#### Paramètres obligatoires pour toutes les requêtes :
* `action` : L'action que l'on veut effectuer
* `uuid_player` : L'uuid du joueur renvoyé par l'API de connexion

#### Les différentes actions

* `action=create` :
  Crée une partie contre l'IA et renvoie son id.<br>
  Paramètres supplémentaires requis :
  * `game_name` : Le nom de la partie.
  * `difficulty_level` : Le niveau de difficulté de l'IA de 1 à 4.
<br>

* `action=play` :
  Joue un coup.  
  Paramètres supplémentaires requis :
  * `id_game` : L'id de la partie où le coup doit être joué
  * `move` : Le coup joué dans le format :

        {
          "piece":"bK",
          "pos":"e8",
          "move":"e7"
        }
     où `bK` signifie black king, `pos` est la position actuelle de la pièce, et `move` est la position de la pièce après le déplacement.

