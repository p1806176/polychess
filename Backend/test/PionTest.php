<?php

use PHPUnit\Framework\TestCase;

require_once 'src/piece.php';

class PionTest extends TestCase {

	public function testCreation() {
		$pion_blanc = new Piece("p", 0, 0, 1);
		$this->assertSame('{"type": "p","team": "white","pos": {"x": 0, "y": 0}}', $pion_blanc->toJSONNoMove());
	}

	/**
	 * @depends testCreation
	 */
	public function testDeplacements() {
		$pion_blanc = new Piece("p", 0, 0, 1);

		$partie = new Partie(null, null, null);
		$partie->setPieces(array($pion_blanc, new Piece("k", 7, 7, 1)));
		// Car erreur si le roi n'est pas dans le plateau

		$this->assertSame(array("01", "XX"), $pion_blanc->getListeCoupsJouables());
	}
	
}