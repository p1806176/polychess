<?php

use PHPUnit\Framework\TestCase;

require_once 'src/partie.php';

class PartieTest extends TestCase {

	public function testConstructeur() {
		$partie = new Partie(null, null, null);

		self::assertIsArray($partie->getPlateau());

		for ($i = 0; $i < 8; $i++) {
			self::assertIsArray($partie->getPlateau()[$i]);

			for ($j = 0; $j < 8; $j++) {
				self::assertSame(null, $partie->getPlateau()[$i][$j]);
			}
		}
	}

	/**
	 * @depends testConstructeur
	 */
	public function testInitialisation() {
		$partie = new Partie(null, null, null);

		$partie->initPartie();

		self::assertSame(32, $partie->getNbPiece());

		self::assertIsArray($partie->getListePieceVivante());

		for ($i = 0; $i < $partie->getNbPiece(); $i++) {
			self::assertInstanceOf("Piece", $partie->getListePieceVivante()[$i]);
		}
	}

	/**
	 * @depends testInitialisation
	 */
	public function testPiecesInitPartie() {
		$partie = new Partie(null, null, null);
		$partie->initPartie();

		$this->piecesPresentes($partie);

		foreach ($partie->getListePieceVivante() as $piece) {
			$piece->setPartie(null);
		}

		self::assertEquals(new Piece("t", 0, 0, 1), $partie->getPlateau()[0][0]);
		self::assertEquals(new Piece("n", 1, 0, 1), $partie->getPlateau()[1][0]);
		self::assertEquals(new Piece("b", 2, 0, 1), $partie->getPlateau()[2][0]);
		self::assertEquals(new Piece("q", 3, 0, 1), $partie->getPlateau()[3][0]);
		self::assertEquals(new Piece("k", 4, 0, 1), $partie->getPlateau()[4][0]);
		self::assertEquals(new Piece("b", 5, 0, 1), $partie->getPlateau()[5][0]);
		self::assertEquals(new Piece("n", 6, 0, 1), $partie->getPlateau()[6][0]);
		self::assertEquals(new Piece("t", 7, 0, 1), $partie->getPlateau()[7][0]);

		self::assertEquals(new Piece("t", 0, 7, 0), $partie->getPlateau()[0][7]);
		self::assertEquals(new Piece("n", 1, 7, 0), $partie->getPlateau()[1][7]);
		self::assertEquals(new Piece("b", 2, 7, 0), $partie->getPlateau()[2][7]);
		self::assertEquals(new Piece("q", 3, 7, 0), $partie->getPlateau()[3][7]);
		self::assertEquals(new Piece("k", 4, 7, 0), $partie->getPlateau()[4][7]);
		self::assertEquals(new Piece("b", 5, 7, 0), $partie->getPlateau()[5][7]);
		self::assertEquals(new Piece("n", 6, 7, 0), $partie->getPlateau()[6][7]);
		self::assertEquals(new Piece("t", 7, 7, 0), $partie->getPlateau()[7][7]);

		for ($i = 0; $i < 8; $i++) {
			self::assertEquals(new Piece("p", $i, 1, 1), $partie->getPlateau()[$i][1]);
			self::assertEquals(new Piece("p", $i, 6, 0), $partie->getPlateau()[$i][6]);
		}
	}

	private function piecesPresentes($partie) {
		$plateauValues = $this->getPlateauValues($partie->getPlateau());

		for ($i = 0; $i < $partie->getNbPiece(); $i++) {
			self::assertContains($partie->getListePieceVivante()[$i], $plateauValues);
			$plateauValues[array_search($partie->getListePieceVivante()[$i], $plateauValues)] = null;
		}

		for ($i = 0; $i < 64; $i++) {
			self::assertNull($plateauValues[$i]);
		}
	}

	private function getPlateauValues($plateau) {
		$plateauValues = array();
		for ($i = 0; $i < 8; $i++) {
			for ($j = 0; $j < 8; $j++) {
				array_push($plateauValues, $plateau[$i][$j]);
			}
		}
		return $plateauValues;
	}

	/**
	 * @depends testPiecesInitPartie
	 */
	public function testDeplacementPionsDebutPartie() {
		$partie = new Partie(null, null, null);
		$partie->initPartie();

		for ($i = 0; $i < 8; $i++) {
			$this->deplacementPiece($partie, "p", $i, 1, $i, 2);
			$this->deplacementPiece($partie, "p", $i, 6, $i, 5);
		}
	}

	private function deplacementPiece(Partie $partie, $typePiece, $depX, $depY, $arrX, $arrY) {
		$team = $partie->getPlateau()[$depX][$depY]->getTeam();

		$partie->jouerCoup($typePiece . $depX . $depY . $arrX . $arrY);

		$this->piecesPresentes($partie);

		self::assertNull($partie->getPlateau()[$depX][$depY]);
		self::assertNotNull($partie->getPlateau()[$arrX][$arrY]);
		$partie->getPlateau()[$arrX][$arrY]->setPartie(null);
		self::assertEquals(new Piece($typePiece, $arrX, $arrY, $team), $partie->getPlateau()[$arrX][$arrY]);
		$partie->getPlateau()[$arrX][$arrY]->setPartie($partie);
	}

}