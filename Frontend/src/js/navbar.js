const divFormulaire = document.querySelector('.navbar .formulaire');
const connectionBtn = document.querySelector('.formulaire .navbar-connectionButton');
const inscriptionBtn = document.querySelector('.formulaire .navbar-inscriptionButton');

const deleteAllChild = function(node){
    const arrayChildren = Array.from(node.children)
    arrayChildren.forEach((child) => child.remove());
}

const isConnected = function(){ 
    const username = sessionStorage.getItem('username');
    if(username !== '')
    return true;
    
    return false;
}


const setDisconnected = function(){
    sessionStorage.setItem('username', '');
    sessionStorage.setItem('uuid_player', '');  
}   


const renderNavbar = function(){
    if(isConnected()) {
        deleteAllChild(divFormulaire);
        
        const newButton = document.createElement('a');
        newButton.setAttribute('href', './connection.html');
        newButton.setAttribute('class', 'btn navbar-disconnectedButton');
        newButton.innerText = "Deconnexion";
        newButton.addEventListener('click', setDisconnected);
        divFormulaire.appendChild(newButton);
    }
}

renderNavbar();


























































































































