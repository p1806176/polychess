const historicDiv = document.querySelector('.informationPanel .informationPanel-historic');
const fetchURL = 'http://giust.ddns.net/backend-echec';

const moveBackendToNormal = function(backendMove){
    const move = {
        piece : '',
        pos : '',
        move : '',
    };

    moveArray = backendMove.split('');

    if(moveArray[0] === moveArray[0].toLowerCase())
        move.piece = `w${moveArray[0].toUpperCase()}`;
    else    
        move.piece = `b${moveArray[0].toUpperCase()}`;

    
}

const fetchHistoric = function() {
    return new Promise((success, fail) => {
        const uuid_player = sessionStorage.getItem('uuid_player');
        const gameId = sessionStorage.getItem('gameId');

        fetch(`${fetchURL}/src/api_game.php?action=history&id_game=${gameId}&uuid_player=${uuid_player}`)
        .then((response) => response.json())
        .then((json) => {
            if(success === 'false') return fail(json.error);

            success(json.moves);
        })
        .catch((err) => console.error(err));
    });
}

fetchHistoric()
.then((movesArray) =>{
    console.log(movesArray);
    movesArray.forEach((el) =>{
        const elSplit = el.split('');
        historicDiv.innerText += `${elSplit[0]} : ${elSplit[1]}${elSplit[2]} - ${elSplit[3]}${elSplit[4]} \n`;
    })
})
.catch((err) => console.error(err));