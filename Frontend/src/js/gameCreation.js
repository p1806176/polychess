const form = document.querySelector('.gameOption-app-option form');
const fetchUrl = "http://giust.ddns.net/backend-echec";

const fetchNewGame = function(name, difficulty) {
    const uuid_player = sessionStorage.getItem('uuid_player');
    console.log(uuid_player);
    const body = `action=create&uuid_player=${uuid_player}&game_name=${name}&difficulty_level=${difficulty}`;
    fetch(`${fetchUrl}/src/api_game.php`, {
        method: "POST",
        headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}),
        body: body,
    })
    .then((response) => response.json())
    .then((json) => {
        if(json.success === false) return console.error('erreur création partie');
        sessionStorage.setItem('gameId', json.id);
        document.location.href="../src/game.html";
    })
    .catch((err) => console.error(err));
}


    
const createNewGame = function(ev) {
    ev.preventDefault();
    const { elements } = ev.target;
    
    const difficulty = elements['difficulty'].value;
    const timePerRound = elements['timePerRound'].value;
    const piecesTheme = elements['piecesTheme'].value;
    const boardTheme = elements['boardTheme'].value;
    const name = elements['nameGame'].value;


    sessionStorage.setItem('difficulty', difficulty);
    sessionStorage.setItem('timePerRound', timePerRound);
    sessionStorage.setItem('defaultPieceTheme', piecesTheme);
    sessionStorage.setItem('defaultBoardTheme', boardTheme);
        
    fetchNewGame(name, difficulty);
}

form.addEventListener('submit', createNewGame);