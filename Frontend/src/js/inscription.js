const formInscription = document.querySelector('.container form');
const fetchUrl = "http://giust.ddns.net/backend-echec";

const fetchRegister = function(body){
        fetch(`${fetchUrl}/src/api_user.php`, {
            method: 'POST',
            headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}),
            body: body,
        }).then((response) => response.json())
        .then((json) => {
            if(json.success === false) 
            console.error(json.error);
            else
            document.location.href="../src/connection.html";
        })
        .catch((err) => console.error(err));
}

const register = function(ev){
    ev.preventDefault();
    const elements = ev.target.elements;
    const username = elements['username'].value;
    const password = elements['password'].value;
    const confirmPassword = elements['confirmpassword'].value;
    if(password === confirmPassword)
    {
        const registerBody = `action=register&username=${username}&password=${password}`;
        fetchRegister(registerBody);    
    }
    else console.error('Passwords are different');

}

formInscription.addEventListener('submit', register);