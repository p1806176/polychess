const table = document.querySelector('table tbody');
const fetchURL = 'http://giust.ddns.net/backend-echec';


const fetchGames = function() {
    return new Promise((success, fail) => {
        const uuid_player = sessionStorage.getItem('uuid_player');
        fetch(`${fetchURL}/src/api_game.php?action=games&uuid_player=${uuid_player}`)
        .then((response) => response.json())
        .then((json) => success(json))
        .catch((err) => fail(err)); 
    });
}

const renderTable = function() {
    fetchGames()
    .then((games) => {
        games.forEach((el) => { 
            console.log(el);                   
            const baliseTr = document.createElement('tr');

            const baliseThId = document.createElement('th');
            baliseThId.setAttribute('scope', 'row');
            baliseThId.innerText = `${el.id}`;

            const baliseTdNom = document.createElement('td');
            baliseTdNom.innerText = `${el.game_name}`;
            
            const baliseTdOppenent = document.createElement('td');
            baliseTdOppenent.innerText = `${el.opponent}`;

            const baliseTdBoutton = document.createElement('td');

            const button = document.createElement('a');
            button.setAttribute('href', './game.html');
            button.setAttribute('class', 'btn btn-secondary');
            button.innerText = "Continuer";

            button.addEventListener('click', () => {
                sessionStorage.setItem('gameId', el.id);
                console.log('dfzqgqe');
            });
            
            baliseTdBoutton.appendChild(button);
    
            baliseTr.appendChild(baliseThId);
            baliseTr.appendChild(baliseTdNom);
            baliseTr.appendChild(baliseTdOppenent);
            baliseTr.appendChild(baliseTdBoutton);
            table.appendChild(baliseTr);
        })
    })
}

renderTable();