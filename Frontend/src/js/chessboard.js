const chessBoard = document.querySelector("#chessboard");
const pieceThemeSelect = document.querySelector("#piecesThemeSelect");
const boardThemeSelect = document.querySelector('#boardTheme');



const fetchUrl = "http://giust.ddns.net/backend-echec";

const config = {};


const backEndBoard2Fen = (backEndBoard) => {
  const tabRes = [];
  const pieces = "p r n b q k P R N B Q K $";
  backEndBoard.split(" ").forEach((element) => {
          if(element != "NULL") {
              tabRes.push(element);
          }
          else{
              if (tabRes.length == 0 || pieces.includes(tabRes[tabRes.length-1])) {
                  tabRes.push("1");
              }     
              else {
                  const nbNULL = parseInt((tabRes[tabRes.length-1]))+1;
                  const nouvelleElement = nbNULL.toString(10);
                  tabRes[tabRes.length-1] = nouvelleElement;
              }
              
          }       
  });
  const tabResMap = tabRes.map(element => (element === "$") ? "/" : element); 
  return tabResMap.join("");
}


const getGameInformation = function() {
  const uuid_player = sessionStorage.getItem('uuid_player');
  const gameId = sessionStorage.getItem('gameId');

  return {
    uuid_player : uuid_player,
    gameId : gameId,
  }
}

const fetchPossibleMoves = function(){
  return new Promise((success, fail) => {
    const {uuid_player, gameId} = getGameInformation();
    fetch(`${fetchUrl}/src/api_game.php?action=moves&uuid_player=${uuid_player}&id_game=${gameId}`)
    .then((response) => response.json())
    .then((json) => success(json))
    .catch((err) => fail(err));
  });
}

const fetchBoard = function (){
  return new Promise((success, fail) => {
    const {uuid_player, gameId} = getGameInformation();
    fetch(`${fetchUrl}/src/api_game.php?action=board&uuid_player=${uuid_player}&id_game=${gameId}`)
    .then((response) => response.json())
    .then((json) => {
      if(json.success === false) return console.error(json.error)

      success(json.board);
    })
    .catch((err) => fail(err));
  })
}

const isMovePossible = function(source, target, piece){
  return new Promise((success, fail) => {
    fetchPossibleMoves()
    .then((moveJson) => {
      if(moveJson.success === false) return console.error(moveJson.error);
    
      const { moves } = moveJson;
      const piecePossibleMoves = moves.reduce((acc, el) => {
        if(el.piece === piece && el.pos === source) 
          return el;
        else 
          return acc;
      });
      
      piecePossibleMoves.moves.forEach((el) => {
        if(el === target)
          return success(true);
      })

      success(false);
    })
    .catch((err) => fail(err));
  })
  
}

const doMove = function(source, target, piece){
  const {uuid_player, gameId} = getGameInformation();

  body = `action=play&uuid_player=${uuid_player}&id_game=${gameId}&move={
    "piece": "${piece}",
    "pos": "${source}",
    "move": "${target}"
  }`;

  fetch(`${fetchUrl}/src/api_game.php`, {
    method: 'POST',
    headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}),
    body: body,
  })
  .then((response) => response.json())
  .then((json) => {
    if(json.success === false) return loadBoard();
  })
  .catch((err) => console.error(err));
}

const loadBoard = function() {
  fetchBoard()
  .then((board) => {
    const fenBoard = backEndBoard2Fen(board);
    config.position = fenBoard;
    board = ChessBoard(chessBoard, config);
    setDefaultBoardTheme();
  })
  .catch((err) => console.error(err));
}

const onDrop = function(source, target, piece) {
  isMovePossible(source, target, piece)
  .then((success) => {
    if(success === false) {
      loadBoard();
    } else {
      doMove(source, target, piece);
    }
  })
  .catch((err) => console.error(err));
}

const getBoardColors = function(boardTheme){
  const colors = {
    color1 : "",
    color2 : ""
  }

  if(boardTheme === 'default') {
    colors.color1 = '#f0d9b5';
    colors.color2 = '#b58863';
  } else if(boardTheme === 'classic'){
    colors.color1 = '#e6e6e6';
    colors.color2 = '#272626';
  } else if (boardTheme === 'blood'){
    colors.color1 = '#ffe6e6';
    colors.color2 = '#ff1a1a';
  } else if (boardTheme === 'emeraude'){
    colors.color1 = '#e6ffee';
    colors.color2 = '#00e64c';    
  } else if (boardTheme === 'fire'){
    colors.color1 = '#e6ffee';
    colors.color2 = '#ff751a';
  } else if (boardTheme === 'forest'){
    colors.color1 = '#e1c284';
    colors.color2 = '#468646';
  } else if (boardTheme === 'ocean'){
    colors.color1 = '#c4d5ed';
    colors.color2 = '#3c72c3';
  } else if (boardTheme === 'asphalte'){
    colors.color1 = '#a8a8a4';
    colors.color2 = '#4e4e4b';
  } else if (boardTheme === 'amethyste'){
    colors.color1 = '#eaccff';
    colors.color2 = '#7a00cc';
  }

  return colors;
}

const setInformationPanelTheme = function(colors){
  const informationPanel = document.querySelector('.game .informationPanel');
  informationPanel.style.backgroundColor = colors.color1;
  
  const playerInformations = document.querySelectorAll('.game .playerInformations');

  playerInformations.forEach((player) => { 
    player.style.backgroundColor = colors.color1;
  });
  
  const informationPanel_historic = document.querySelector('.game .informationPanel-historic');
  informationPanel_historic.style.backgroundColor = colors.color2;
  
  const informationPanel_optionButtons = document.querySelector('.game .informationPanel-optionButtons');
  informationPanel_optionButtons.style.backgroundColor = colors.color2;
}


const setBoardTheme = function(boardTheme){
  const whiteSquare = document.querySelectorAll('#chessboard .white-1e1d7');
  const blackSquare = document.querySelectorAll('#chessboard .black-3c85d');

  const colors = getBoardColors(boardTheme);

  setInformationPanelTheme(colors);

  whiteSquare.forEach((square) => {
    square.style.backgroundColor = colors.color1;
    square.style.color = colors.color2;
  });
  
  blackSquare.forEach((square) => {
    square.style.backgroundColor = colors.color2;
    square.style.color = colors.color1;
  });

  sessionStorage.setItem('defaultBoardTheme', boardTheme);
}

const setDefaultBoardTheme = function(){
  const defaultBoardTheme = sessionStorage.getItem('defaultBoardTheme');
  setBoardTheme(defaultBoardTheme);
}

const setDefaultConfig = function(config) {
  config.draggable = true;
  config.dropOffBoard = 'snapback';
  config.position = 'start';
  config.onDrop = onDrop;

  const defaultPieceTheme = sessionStorage.getItem('defaultPieceTheme');
  config.pieceTheme = `../img/chesspieces/${defaultPieceTheme}/{piece}.png`;

  setDefaultBoardTheme();
}


const setNewBoardTheme = function(ev){
  const { value } = ev.target
  setBoardTheme(value);
}

const setNewPiecesTheme = function(ev) {
  const newPiecesTheme = ev.target.value;
  config.pieceTheme = `../img/chesspieces/${newPiecesTheme}/{piece}.png`;
  board = ChessBoard(chessBoard, config);
  setDefaultBoardTheme();
}

const loadGame = function() {
  setDefaultConfig(config);
  let board = ChessBoard(chessBoard, config);
  setDefaultBoardTheme();
  loadBoard();
  
  pieceThemeSelect.addEventListener('change', setNewPiecesTheme);
  boardThemeSelect.addEventListener('change', setNewBoardTheme);
}

loadGame();





  