const timePerround = sessionStorage.getItem('timePerround');

const onStart = function()
{
    const start_player1 = new Date().getTime();
    const start_player2 = new Date().getTime();
    const player = 'white';
    const start = 'start';
    sessionStorage.setItem('start_player1' , start_player1); 
    sessionStorage.setItem('start_player2' , start_player2);
    sessionStorage.setItem('player' , player);
}




const ChangePlayer = function()
{
    if(sessionStorage.getItem('player') === 'white')
    {
        const start_player1 = new Date().getTime();
        const player = 'black';
        sessionStorage.setItem('start_player1' , start_player1);
        sessionStorage.setItem('player' , player);
    }
    else
    {
        const start_player2 = new Date().getTime();
        const player = 'white';
        sessionStorage.setItem('start_player2' , start_player2);
        sessionStorage.setItem('player' , player)
    }
}



const timer1 = function()
{
    const now1 = new Date().getTime();
    const timerPlayer1 = now1 - sessionStorage.getItem('start_player1');
    const minutes_player1 = Math.floor((timerPlayer1 % (1000 * 60 * 60)) / (1000 * 60));
    const seconds_player1 = Math.floor((timerPlayer1 % (1000 * 60)) / 1000);
    let display_seconds_player1 = "";
    if(seconds_player1<10)
    {
        display_seconds_player1 = "0" + seconds_player1;
    }
    else
    {
        display_seconds_player1 = seconds_player1 + "";
    }
    const t1 = document.getElementById('timerPlayer1').textContent = minutes_player1 + ":" + display_seconds_player1;
    if((minutes_player1===0)&&(seconds_player1===5))
    {
        ChangePlayer();
    }
}

const timer2 = function()
{
    const now2 = new Date().getTime();
    const timerPlayer2 = now2 - sessionStorage.getItem('start_player2');
    const minutes_player2 = Math.floor((timerPlayer2 % (1000 * 60 * 60)) / (1000 * 60));
    const seconds_player2 = Math.floor((timerPlayer2 % (1000 * 60)) / 1000);
    let display_seconds_player2 = "";
    if(seconds_player2<10)
    {
        display_seconds_player2 = "0" + seconds_player2;
    }
    else
    {
        display_seconds_player2 = seconds_player2 + "";
    }
    const t2 = document.getElementById('timerPlayer2').textContent = minutes_player2 + ":" + display_seconds_player2;
    if((minutes_player2===0)&&(seconds_player2===5))
    {
        ChangePlayer();
    }
}

const timer = function()
{
    if(sessionStorage.getItem('player') === 'white')
    {
        timer1();
    }
    else
    {
        timer2();
    }

}

onStart();


setInterval(timer,100);

    