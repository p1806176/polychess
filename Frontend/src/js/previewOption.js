const piecesThemeSelect = document.querySelector('#piecesTheme');
const imgsPiecesThemePreview = document.querySelectorAll('.app-option-img .pieces img');

const boardThemeSelect = document.querySelector('#boardTheme');
const imgBoardThemePreview = document.querySelector('.app-option-img .theme img');

const setNewPiecesPreview = function(ev) {
    const newPiecesThemes = ev.target.value;

    imgsPiecesThemePreview.forEach((img) => {
        const piece = img.id;
        img.src = `../img/chesspieces/${newPiecesThemes}/${piece}.png`;
    })
}

const setNewBoardPreview = function(ev) {
    const newBoardTheme = ev.target.value;
    imgBoardThemePreview.src = `../img/theme/${newBoardTheme}.png`;
}

piecesThemeSelect.addEventListener('change', setNewPiecesPreview);
boardThemeSelect.addEventListener('change', setNewBoardPreview);
