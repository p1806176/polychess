const formConnection = document.querySelector('.container .form-signin');
const fetchUrl = "http://giust.ddns.net/backend-echec";

const fetchConnection = function(bodyConnection) {
    fetch(`${fetchUrl}/src/api_user.php`, {
        method: 'POST',
        headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}),
        body: bodyConnection,
    }).then((response) => response.json())
    .then((json) => {
        if(json.success === false) return console.error(json.error) ;
        
        sessionStorage.setItem('uuid_player', json.uuid);
        sessionStorage.setItem('username', json.username);
        document.location.href="../src/gameOption.html";

    })
    .catch((err) => console.error(err));
}

const getConnected = function(ev) {
    ev.preventDefault();
    const elements = ev.target.elements;

    const username = elements['username'].value;
    const password = elements['password'].value;
    const bodyConnection = `action=login&username=${username}&password=${password}`;
    
    fetchConnection(bodyConnection);
}

formConnection.addEventListener('submit', getConnected);

 