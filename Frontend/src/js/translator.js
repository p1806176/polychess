const chaineTest = "r NULL b q k n r \n p p p p NULL p p p \n NULL NULL n NULL NULL NULL NULL NULL \n NULL B NULL NULL p NULL NULL NULL \n NULL NULL NULL NULL P NULL NULL NULL \n NULL NULL NULL NULL NULL N NULL NULL \n P P P P NULL P P P \n R N B Q K NULL NULL R";
const chaineTest2 = "r NULL NULL NULL p";
const chaineTest3 = "T N B Q K B N T \n P P P P P P P P \n NULL NULL NULL NULL NULL NULL NULL NULL \n NULL NULL NULL NULL NULL NULL NULL NULL \n NULL NULL NULL NULL NULL NULL NULL NULL \n NULL NULL NULL NULL NULL NULL NULL NULL \n p p p p p p p p \n t n b q k b n t"
const chaineTest4 ="r n b q k b n r \n p p p p p p p p \n NULL NULL NULL NULL NULL NULL NULL NULL \n NULL NULL NULL NULL NULL NULL NULL NULL \n NULL NULL NULL NULL NULL NULL NULL NULL \n NULL NULL NULL NULL NULL NULL NULL NULL \n P P P P P P P P  \n R N B Q K B N R"
const backEndBoard2Fen = (backEndBoard) => {
                            const tabRes = [];
                            const pieces = "p r n b q k t P R N B Q K T \n";
                            backEndBoard.split(" ").forEach((element) => {
                                    if(element != "NULL") {
                                        tabRes.push(element);
                                    }
                                    else{
                                        if (tabRes.length == 0 || pieces.includes(tabRes[tabRes.length-1])) {
                                            tabRes.push("1");
                                        }     
                                        else {
                                            const nbNULL = parseInt((tabRes[tabRes.length-1]))+1;
                                            const nouvelleElement = nbNULL.toString(10);
                                            tabRes[tabRes.length-1] = nouvelleElement;
                                        }
                                        
                                    }       
                            });
                            const tabResMap = tabRes.map(element => (element === "\n") ? "/" : element); 
                            return tabResMap.join("");
                        }

console.log(backEndBoard2Fen(chaineTest4));