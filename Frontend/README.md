# Projet d'échecs : Frontend 

## Présentation
C'est un projet dans le cadre de notre fin de deuxieme année polytech et qui sonne par la même occasion la fin de nos deux années preparatoires. Ce projet consiste en la conception d'un jeu d'echecs avec la collaboration de 3 groupes que sont le frontend (nous), le backend et le groupe IA. Notre role dans ce projet consiste en la mise en place d'une interface fonctionnel et responsive permettant de lancer la partie et d'interagir avec celle-ci etc.

## Utilisation

`inscription.html` et `connexion.html` : Pour lancer le jeu, il suffit de lancer la page inscription.html et de s'inscrire avec un pseudo et un mot de passe valide sinon un message d'erreur sera renvoyé. Cela fait, il vous suffira de vous connecter avec l'identifiant et le mot de passe choisis précédemment. 

`gameOption.html` : Une fois ces deux actions faites vous pourrez jouer au jeu et en definir les parametres tel que la difficulté de la partie, le thème du plateau et des pièces ainsi que le nom de la partie permettant de référencer celle-ci dans un historique de partie dedié.

`game.html` : il est possible de changer le theme du plateau et des pièces en cliquant sur le rouage en dessous de l'historique.

`rules.html` : cette page est présente pour les novices afin qu'ils puissent s'informer des différentes règles du jeu, que ce soit le mouvement de chaque pièce, ou encore les règles avancées. Il suffit d'appuyer sur le boutton de la pièce ou la règle qui vous interesse pour être redirigé vers l'explication.   

## Lien avec le backend
Par defaut le frontend est lié au serveur mis en place par le backend qui est : 'http://giust.ddns.net/backend-echec'
Cependant si vous souhaitez utiliser votre propre serveur cela est possible en changeant les paramètres de fetchUrl dans :
 * `table.js`
 * `historic.js`
 * `inscription.js`
 * `connection.js`
 * `gameCreation.js`

 Attention le code part du principe qu'à partir du fetchUrl les apis sont dans src/api_game.php et src/api_user.php !

#### Auteurs
* `BAUVENT Kilian`
* `AHMED KHAN Somir`
* `ABDELGHANI Tarek`
* `GREINCH Imrane`
* `BERTRAND Adrien`




